import React, { useContext } from "react";
import { SharedDataContext } from "../components/app/UseContext";
import MicroFrontend from "../MicroFrontend";
const Dashboard = ({ history }) => {
  const { sharedDataContext, setSharedDataContext } = useContext(
    SharedDataContext
  );

  /*const Dashboard = () => {
        return <div>Dashboard</div>;
    };*/
  return (
    <MicroFrontend
      history={history}
      host={sharedDataContext.links.dashboard}
      name="Dashboard"
    />
  );
};

export default Dashboard;
