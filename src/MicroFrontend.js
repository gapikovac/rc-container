import React from "react";

class MicroFrontend extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false
    };
  }

  componentDidMount() {
    const { name, host, document } = this.props;
    const scriptId = `micro-frontend-script-${name}`;

    if (document.getElementById(scriptId)) {
      this.renderMicroFrontend();
      return;
    }

    fetch(`${host}/asset-manifest.json`)
      .then(res => res.json())
      .then(manifest => {
        const script = document.createElement("script");
        script.id = scriptId;
        script.crossOrigin = "";
        script.src = `${host}${manifest["main.js"]}`;
        script.onload = this.renderMicroFrontend;
        document.head.appendChild(script);
      })
      .catch(error => {
        this.setState({ error: true });
        console.log("error", error);
      });
  }

  componentWillUnmount() {
    if (!this.state.error) {
      const { name, window } = this.props;
      window[`unmount${name}`](`${name}-container`);
    }
  }

  renderMicroFrontend = () => {
    const { name, window, history } = this.props;
    window[`render${name}`](`${name}-container`, history);
  };

  render() {
    const { error } = this.state;
    return error ? (
      <h1>Error while loading microfrontend [{this.props.host}]</h1>
    ) : (
      <main id={`${this.props.name}-container`} />
    );
  }
}

MicroFrontend.defaultProps = {
  document,
  window
};

export default MicroFrontend;
