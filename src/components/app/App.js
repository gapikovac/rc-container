import React, { useState, useMemo, useEffect } from "react";
import { withTranslation } from "react-i18next";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../shards-dashboard/styles/shards-dashboards.1.1.0.min.css";
import "../../assets/styles/bluma.scss";
import DefaultLayout from "../layouts/Default";
import { SharedDataContext } from "./UseContext";
import MicroFrontend from "../../MicroFrontend";
import useHttpService from "../../services/HttpService";
import RenderWithLayout from "./RenderWithLayout";
import { MICROFRONTEND_VERSION_LINKS } from "../../constants/Constants";
import Dashboard from "../../microfrontends/Dashboard";

const App = props => {
  const Tickets = () => {
    return <div>Tickets</div>;
  };

  const Settings = () => {
    return <div>Settings</div>;
  };

  const Welcome = () => {
    return <div>Welcome</div>;
  };

  //chechsession request
  const {
    send: sendRequestCheckSession,
    data: sessionResponse
  } = useHttpService("CHECK_SESSION");

  const [user, setUser] = useState(null);

  const [sharedDataContext, setSharedDataContext] = useState({
    t: props.t,
    i18n: props.i18n,
    socketConnected: false,
    userLogged: false,
    status: 0,
    currentUser: {},
    notification: {
      active: false, // false , true
      status: "", // success , danger,
      content: { title: "", msg: "" }
    }
  });

  const providerSharedDataContext = useMemo(
    () => ({ sharedDataContext, setSharedDataContext }),
    [sharedDataContext, setSharedDataContext]
  );

  //check session useEffect
  useEffect(() => {
    checkSession();
  }, []);

  //
  useEffect(() => {
    if (sessionResponse) {
      if (sessionResponse.status === 200) {
        // user session is ok
        // set user session
        updateUserInfo(sessionResponse.user);
      }
    }
  }, [sessionResponse]);

  function checkSession() {
    sendRequestCheckSession();
  }

  function updateUserInfo(u) {
    setUser(u);
    const links = {
      onboard:
        MICROFRONTEND_VERSION_LINKS.ONBOARD[u.rc_microfrontend_version.onboard],
      dashboard:
        MICROFRONTEND_VERSION_LINKS.DASHBOARD[
          u.rc_microfrontend_version.dashboard
        ],
      tickets:
        MICROFRONTEND_VERSION_LINKS.TICKETS[u.rc_microfrontend_version.tickets],
      settings:
        MICROFRONTEND_VERSION_LINKS.SETTINGS[
          u.rc_microfrontend_version.settings
        ]
    };
    setSharedDataContext({
      ...sharedDataContext,
      currentUser: u,
      userLogged: true,
      socketConnected: true,
      status: 0,
      links: links
    });
  }

  return (
    <Router>
      <SharedDataContext.Provider value={providerSharedDataContext}>
        {user ? (
          <Switch>
            <Route
              exact
              path={"/"}
              render={() => (
                <RenderWithLayout Component={Welcome} Layout={DefaultLayout} />
              )}
            />
            <Route
              path={"/dashboard"}
              render={() => (
                <RenderWithLayout
                  Component={Dashboard}
                  Layout={DefaultLayout}
                />
              )}
            />
            <Route
              path={"/tickets"}
              render={() => (
                <RenderWithLayout Component={Tickets} Layout={DefaultLayout} />
              )}
            />
            <Route
              path={"/settings"}
              render={() => (
                <RenderWithLayout Component={Settings} Layout={DefaultLayout} />
              )}
            />
          </Switch>
        ) : (
          <></>
        )}
      </SharedDataContext.Provider>
    </Router>
  );
};

export default withTranslation()(App);
