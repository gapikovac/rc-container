import React, { useState, useEffect, useContext } from "react";
import { SharedDataContext } from "./UseContext";
import useRouter from "./useRouter";
import { check_onboardingHttpService } from "../../services/HttpService";
import Loader from "../common/Loader";

const RenderWithLayout = ({ Component, Layout }) => {
  const path = window.location.pathname;
  const { sharedDataContext } = useContext(SharedDataContext);
  const router = useRouter();
  const [stateCheckOnBoarding, setstateCheckOnBoarding] = useState(false);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    checkOnBoarding();
  }, []);

  function checkOnBoarding() {
    const receptor = {
      headers: {
        "Content-Type": "application/json",
        publickey:
          sharedDataContext && sharedDataContext.currentUser
            ? sharedDataContext.currentUser.publickey
            : false,
        apisid:
          sharedDataContext && sharedDataContext.currentUser
            ? sharedDataContext.currentUser.apisid
            : false,
        sessionid:
          sharedDataContext && sharedDataContext.currentUser
            ? sharedDataContext.currentUser.session_id
            : false
      }
    };

    check_onboardingHttpService.check_onboarding(receptor).then(response => {
      if (response.status === 200 || response.status === 202) {
        if (response.data.state) router.push("/dashboard");
        setLoading(false);
        setstateCheckOnBoarding(response.data.state);
      } else {
        // console.log("test error : ", response);
      }
    });
  }

  return loading ? (
    <Loader width={"60"} height={"60"} />
  ) : (
    <Layout noFooter={path === "/"} noMenubar={path === "/"}>
      <Component />
    </Layout>
  );
};

export default RenderWithLayout;
