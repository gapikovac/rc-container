import React, { Component } from "react";
import PropTypes from "prop-types";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import trashIcon from "../../../assets/images/tickets/table-list/trash.svg";
import ProfileIcon from "../../../assets/images/profile/user.svg";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Modal from "../tickets/Modal";
//format date
const dateFormat = date => {
  const regex = /-/gi;

  return date.substring(0, 10).replace(regex, "/");
};
class TicketsList extends Component {
  constructor(props) {
    super(props);

    const { t } = this.props;

    this.state = {
      openModaldelet: false,
      ticketId: "",
      id: "",
      allTicketCheck: [],
      AllticketSelected: false,
      ckecked: false,
      actualPage: 0
    };
  }
  deleteTicket = (elmt, index) => {
    // console.log(elmt);
    //console.log(index);
  };

  openModal = (ticketId, id) => {
    this.setState({ openModaldelet: true, ticketId, id });
  };

  handleClose = () => {
    this.setState({ openModaldelet: false });
  };

  handleCloseAccepte = () => {
    this.setState({ openModaldelet: false });
    this.props.deleteTicket(this.state.ticketId, this.state.id);
    this.setState({
      actualPage: this.state.actualPage + 1
    });
  };

  componentWillUpdate() {}

  componentWillReceiveProps = nextProps => {
    if (nextProps === undefined) {
      return false;
    } else {
      // allTickets = nextProps.allTickets;
      // console.log(
      //   " this.state.allTicketCheck",
      //   this.state.allTicketCheck.length > 0
      //     ? this.state.allTicketCheck[this.state.allTicketCheck.length - 1]
      //         .ticket
      //     : false
      // );
      // console.log("nextProps.allTickets", nextProps.allTickets);
      if (
        this.state.actualPage === nextProps.page &&
        this.state.allTicketCheck.length > 0 &&
        nextProps.allTickets.length &&
        this.state.allTicketCheck[this.state.allTicketCheck.length - 1]
          .ticket === nextProps.allTickets[nextProps.allTickets.length - 1].id
      ) {
        // )
        this.setState({
          allTicketCheck: this.state.allTicketCheck
        });
      } else {
        this.setState({
          actualPage: nextProps.page,
          allTicketCheck: nextProps.allTickets.map(elm => {
            return {
              ticket: elm.id,
              isChecked: false
            };
          })
        });
      }
    }
  };

  handleChange = event => {
    //  console.log("event", event.target.value + ":" + event.target.checked);
    let prio = event.target.value;
    let bool = event.target.checked;
    //console.log(prio, "/ ", bool);
    //  this.setState({ [event.target.value]: event.target.checked }, () => {
    if (prio === "AllticketSelected") {
      let allTicketCheck = this.state.allTicketCheck;
      allTicketCheck.forEach(ticket => (ticket.isChecked = bool));
      this.setState({
        allTicketCheck
      });
    } else {
      this.setState({
        allTicketCheck: this.state.allTicketCheck.map(ticket =>
          ticket.ticket === prio ? { ticket: prio, isChecked: bool } : ticket
        )
      });
    }
    // });
  };

  handleCheckChieldElement = event => {
    // console.log(event.target.checked);
    // console.log("allTicketChecState", this.state.allTickets);
    // let allTicketChec = this.state.allTicketChec;
    // console.log('allTicketChec',allTicketChec)
    // allTicketChec.forEach(elm => {
    //   if (elm.ticket === event.target.value)
    //   elm.isChecked = event.target.checked;
    // });
    // this.setState({ allTicketChec: allTicketChec });
  };

  render() {
    const { t, handleMessageTicket, allTickets } = this.props;
    const { allTicketCheck } = this.state;
    // console.log("allTickets", allTickets);
    const TicktsList = allTickets
      ? allTickets.map(elm => {
          return {
            id: elm.number,
            title: elm.subject,
            priority:
              elm.priority.name === "Medium" || elm.priority.name === "Moyenne"
                ? t("settings.tickets_priority_content.medium")
                : elm.priority.name === "Low" || elm.priority.name === "Faible"
                ? t("settings.tickets_priority_content.low")
                : t("settings.tickets_priority_content.hight"),
            created: elm.created_by.firstname + " " + elm.created_by.firstname,
            status:
              elm.status.name === "Resolved" || elm.status.name === "Résolu"
                ? t("settings.tickets_status_content.resolved")
                : elm.status.name === "Pending" ||
                  elm.status.name === "En attente"
                ? t("settings.tickets_status_content.pending")
                : t("settings.tickets_status_content.new"),
            idTicket: elm.id,
            created_at: elm.created_at
          };
        })
      : [];
    // console.log("TicktsList", TicktsList);
    const selectRow = {
      mode: "checkbox",
      clickToSelect: true
    };
    const options = {
      pageStartIndex: 1,
      sizePerPage: 7,
      hideSizePerPage: true,
      hidePageListOnlyOnePage: true,
      nextPageText: t("tickets.table_list.nextPage"),
      prePageText: t("tickets.table_list.prePage")
    };

    const productView =
      TicktsList.length > 0 && allTicketCheck && allTicketCheck.length > 0
        ? TicktsList.map((elmt, index) => {
            return (
              <div
                key={elmt.idTicket}
                className="liste-item-contain"
                style={{
                  background: `${index % 2 === 0 ? "#FAFBFD" : "#FFFFFF"}`,
                  borderBottom: `${
                    index === TicktsList.length - 1
                      ? "none"
                      : "1px solid #C8D3D6"
                  }`
                }}
              >
                {" "}
                {this.state.allTicketCheck.map(elm => {
                  return elm.ticket === elmt.idTicket ? (
                    <FormGroup row key={elm.ticket}>
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={elm.isChecked}
                            onChange={this.handleChange}
                            value={elm.ticket}
                            color="primary"
                          />
                        }
                        label=""
                      />
                    </FormGroup>
                  ) : (
                    false
                  );
                })}
                <span style={{ flex: "1" }}>#{elmt.id}</span>
                <p
                  style={{
                    textDecoration: "underline",
                    cursor: "pointer",
                    flex: "2"
                  }}
                  onClick={() =>
                    handleMessageTicket(
                      "show-ticket-detail",
                      "----",
                      elmt.idTicket
                    )
                  }
                >
                  {elmt.title && elmt.title.length > 24
                    ? elmt.title.substring(0, 20) + "..."
                    : elmt.title}
                </p>
                <div style={{ flex: "1" }}>
                  <p
                    className="list-ticket-textpriority"
                    style={{
                      background: `${
                        elmt.priority === "High" || elmt.priority === "Haute"
                          ? "#eb592321"
                          : elmt.priority === "Low" ||
                            elmt.priority === "Faible"
                          ? "#6572884a"
                          : "#ff9b214d"
                      }`,
                      color: `${
                        elmt.priority === "High" || elmt.priority === "Haute"
                          ? "#EB5923"
                          : elmt.priority === "Low" ||
                            elmt.priority === "Faible"
                          ? "#657288"
                          : "#FF9B21"
                      }`
                    }}
                  >
                    {elmt.priority}
                  </p>
                </div>
                <div style={{ flex: "1", textAlign: "center" }}>
                  <img
                    src={ProfileIcon}
                    alt="profile picture"
                    className="profilepicture-assignee"
                  />
                </div>
                <div
                  style={{ flex: "1", display: "flex", alignItems: "center" }}
                >
                  <button
                    className="list-ticket-btn"
                    style={{
                      background: `${
                        elmt.status === "Pending" ||
                        elmt.status === "En attente"
                          ? "#FF9B21"
                          : elmt.status === "New" || elmt.status === "Nouveau"
                          ? "#0089E1"
                          : "#00BD39"
                      }`
                    }}
                  />
                  {elmt.status}
                </div>
                {/* <div style={{paddingRight:"25px"}}>
            {dateFormat(elmt.created_at)}
          </div> */}
                <div style={{ flex: "1", display: "flex" }}>
                  <button
                    className="button"
                    onClick={() =>
                      handleMessageTicket(
                        "show-ticket-detail",
                        "----",
                        elmt.idTicket
                      )
                    }
                  >
                    {t("tickets.table_list.btn_view")}
                  </button>

                  <button
                    className="button button-trash-table no-padding"
                    onClick={() => this.openModal(elmt.idTicket, elmt.id)} //() => }
                  >
                    {/* <object type="image/svg+xml" data={trashIcon} className="img-trash" onClick={() => this.deleteTicket(elmt, index)}>
            </object> */}
                    <img
                      src={trashIcon}
                      alt="img trash"
                      className="img-trash"
                      width="15"
                    />
                  </button>
                </div>
              </div>
            );

            //     {/* <BootstrapTable
            //       keyField="id"
            //       data={productView}
            //       columns={this.state.columns}
            //       selectRow={selectRow}
            //       pagination={paginationFactory(options)}
            //     /> */}
            //   </div>
            // );
          })
        : false;
    return (
      <div style={{ padding: "0" }}>
        <div className="head-list-ticket">
          <FormGroup row>
            <FormControlLabel
              control={
                <Checkbox
                  checked={this.state.ticket}
                  onChange={this.handleChange}
                  value="AllticketSelected"
                  color="primary"
                />
              }
              label=""
            />
          </FormGroup>
          <span className="head-list-Title">
            {t("tickets.table_list.ticket_id")}
          </span>
          <span className="head-list-Title" style={{ flex: "2" }}>
            {t("tickets.table_list.ticket_title")}
          </span>
          <span className="head-list-Title">
            {t("tickets.table_list.ticket_priority")}
          </span>
          <span className="head-list-Title" style={{ textAlign: "center" }}>
            AGENT
          </span>

          <span className="head-list-Title">
            {t("tickets.table_list.status")}
          </span>
          {/* <span className="head-list-Title" style={{ textAlign: "center" }}>
            DATE
          </span> */}
          <span className="head-list-Title" style={{ textAlign: "center" }}>
            {t("tickets.table_list.action")}
          </span>
        </div>
        <div className="list-ticket-container">
          {allTickets.length === 0 ? (
            <div className="no-data-contain">
              {t("tickets.table_list.no-data")}
            </div>
          ) : (
            productView
          )}
        </div>
        <Modal
          t={this.props.t}
          handleClickOpen={this.state.openModaldelet}
          handleCloseAccepte={() => this.handleCloseAccepte()}
          handleClose={() => this.handleClose()}
        />
      </div>
    );
  }
}
TicketsList.propTypes = {
  t: PropTypes.func.isRequired,
  handleMessageTicket: PropTypes.func.isRequired
};

export default TicketsList;
