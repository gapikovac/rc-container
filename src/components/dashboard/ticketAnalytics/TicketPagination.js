import React from "react";

const TicketPagination = ({
  ticketInfot,
  nextPage,
  previousPage,
  gotToPage,
  t
}) => {
  const listPagination = nbpagination => {
    let tab = [];
    for (let i = 1; i <= nbpagination; i++) {
      tab.push(i);
    }
    return tab.map((elm, i) => (
      <button
        key={i}
        className="pageNumberbtn"
        onClick={() => gotToPage(i + 1)}
        style={{
          background: `${ticketInfot.page === i + 1 ? "#C8D3D6" : "#FFFFFF"}`
        }}
      >
        {elm}
      </button>
    ));
  };
  return (
    <div className="pagination-container">
      <button
        className="previousButton"
        onClick={previousPage}
        disabled={ticketInfot.total_pages === 1 || ticketInfot.page === 1}
        style={{
          color: `${
            ticketInfot.total_pages === 1 || ticketInfot.page === 1
              ? "#C8D3D6"
              : "#4C4C4C"
          }`
        }}
      >
        {t("tickets.table_list.prePage")}
      </button>
      <div>{listPagination(ticketInfot.total_pages)}</div>
      <button
        className="nextButton"
        onClick={nextPage}
        disabled={
          ticketInfot.total_pages === 1 ||
          ticketInfot.page === ticketInfot.total_pages
        }
        style={{
          color: `${
            ticketInfot.total_pages === 1 ||
            ticketInfot.page === ticketInfot.total_pages
              ? "#C8D3D6"
              : "#4C4C4C"
          }`
        }}
      >
        {t("tickets.table_list.nextPage")}
      </button>
    </div>
  );
};

export default TicketPagination;
