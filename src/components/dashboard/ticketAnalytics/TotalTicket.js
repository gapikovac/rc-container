import React from "react";
import PropTypes from "prop-types";
import DatePicker from "../../common/DatePicker";

const TotalTicket = props => {
  const {
    t,
    MoreIcon,
    CalendarIcon,
    last,
    ticketStats,
    ticketTotalDateFilter
  } = props;

  return (
    <div className="column dashbordticket-column">
      <div className="card analytics-card-parent analytics-card-parent-new-request">
        <div className="card-content analytics-card">
          <p className="subtitle">{t("tickets.total_ticket")}</p>
          <div className="title title-manageticket">
            <span className="statistic-ticket">{ticketStats}</span>
            <div className="arrawbtn" style={{ background: "#eb592352" }}>
              <button
                className="uptriangle"
                style={{
                  background: "#EB5923",
                  border: "none",
                  margin: "auto"
                }}
              ></button>
            </div>
          </div>
          <div className="button is-secondary is-outlined">
            {/* <img className="view-more" src={MoreIcon} alt='caneldar' /> */}
            <DatePicker
              t={t}
              getTimeFilter={(start, end) => ticketTotalDateFilter(start, end)}
            />
            <img className="calendar-icon" src={CalendarIcon} alt="caneldar" />
          </div>
        </div>
      </div>
    </div>
  );
};

TotalTicket.propTypes = {
  t: PropTypes.func.isRequired
};

export default TotalTicket;
