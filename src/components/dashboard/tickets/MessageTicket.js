import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import Select from "react-select";
import moment from "moment";
import User from "../../../assets/images/tickets/user.svg";
import Envelope from "../../../assets/images/tickets/envelope.svg";
import SearchIcon from "../../../assets/images/profile/search.svg";
import ProfileIcon from "../../../assets/images/profile/user.svg";
import Phone from "../../../assets/images/tickets/phone-call.svg";
import ArrowDown from "../../../assets/images/tickets/full-down-arrow.svg";
import DownloadIcon from "../../../assets/images/tickets/DownloadIcon.svg";
import { SharedDataContext, DataSocketContext } from "../../app/UseContext";
import Footer from "../../layouts/Footer";
/* START $$$$$$$$$$$$$$$$$$$$$$$$$$$$$ */
import {
  GetAllTicketsHttpService,
  CreateTicketHttpService,
  UpdateTicketHttpService
} from "../../../services/HttpService";
import {
  SOCKET,
  SIO_TICKET_SETTINGS,
  SIO_GET_TICKET_DETAILS,
  SIO_AGENT_PLATFORM,
  SIO_TICKET_UPDATE
} from "../../../constants/Constants";
import { CONSTANT } from "../../../constants/browser";

const ticketsLog = [
  {
    agent: ProfileIcon,
    activity: "change ticket proprity",
    date: "1 min ago"
  },
  {
    agent: ProfileIcon,
    activity: "Change Ticket Status",
    date: "1 min ago"
  },
  {
    agent: ProfileIcon,
    activity: "Tag Jean-Julian & Venance to ticket",
    date: "1 min ago"
  },
  {
    agent: ProfileIcon,
    activity: "Assign himself to ticket",
    date: "1 min ago"
  },
  {
    agent: ProfileIcon,
    activity: "Create Ticket #34421231",
    date: "1 min ago"
  }
];
// Calculate Time passed from Date of the last update

//Render Agent Name on blue
const updateMessage = message => {
  // console.log("message", message);
  let tab = message.split(" ");
  //console.log("message", tab);
  if (tab[0] === "Assigned") {
    tab[1] = `<span class="agentName">${tab[1]}</span>`;
    tab[2] = `<span class="agentName">${tab[2]}</span>`;
  } else if (tab[1] === "assigné") {
    tab[2] = `<span class="agentName">${tab[2]}</span>`;
    tab[3] = `<span class="agentName">${tab[3]}</span>`;
  }

  return <div dangerouslySetInnerHTML={{ __html: tab.join(" ") }} />;
};

const updateMessage2 = message => {
  let list = message.split("<ul>");
  let text = [];
  // console.log('message',message)
  //console.log('list',list)
  if (message.indexOf("<ul>") === -1 || list.length === 2) {
    return <div dangerouslySetInnerHTML={{ __html: message }} />;
  } else {
    //console.log(list)
    for (let i of list) {
      text.push(<div dangerouslySetInnerHTML={{ __html: i }} />);
    }
    //console.log(text)
    return text.join(" ");
    //let text = <div dangerouslySetInnerHTML={{ __html: message }} />
    //<div dangerouslySetInnerHTML={{ __html: message }} />;
  }
};

function logout(file) {
  let url = `https://cdn.rightcomtech.com/api/1.0/download?app_name=rightcare&file_id=${file}&as_attachment=0`;
  return window.open(url);
}

function replaceMessage(str) {
  const regex = /<p>/gi;
  const regex1 = /[</p>]/gi;
  return str.replace(regex, "").replace(regex1, "");
}

class MessageTicket extends Component {
  constructor(props) {
    super(props);

    const { t } = props;

    this.state = {
      lang: "",
      displayMessage: false,
      assegneeModalOpen: false,
      initAgents: [],
      storeInitAgents: [],
      multiValue: "High",
      multiValuestat: "Pending",
      userData: {},
      addAgent: [],
      fileTicket: [],
      searchAgent: "",
      priority: [
        {
          value: "High",
          label: t("tickets.details_ticket.priority_select.high"),
          index: "High"
        },
        {
          value: "Medium",
          label: t("tickets.details_ticket.priority_select.medium"),
          index: "Medium"
        },
        {
          value: "Low",
          label: t("tickets.details_ticket.priority_select.low"),
          index: "Low"
        }
      ],
      status: [
        {
          value: "Pending",
          label: t("tickets.details_ticket.status_select.pending"),
          index: "Pending"
        },
        {
          value: "Resolved",
          label: t("tickets.details_ticket.status_select.resolved"),
          index: "Resolved"
        },
        {
          value: "New",
          label: t("tickets.details_ticket.status_select.new"),
          index: "New"
        }
      ],
      ticket: {},
      ticketStatus: {},
      ticketPriority: {},
      assigned_agent: {},
      ticketId: ""
    };
  }

  static contextType = SharedDataContext;
  //Close modal every where Click
  componentDidMount() {
    const context = this.context;
    const receptor = {
      headers: {
        "Content-Type": "application/json",
        publickey: context.sharedDataContext.currentUser.publickey,
        apisid: context.sharedDataContext.currentUser.apisid,
        sessionid: context.sharedDataContext.currentUser.session_id
      }
    };
    this.setState({
      userData: context.sharedDataContext.currentUser,
      lang: context.sharedDataContext.defaultLang.value
    });
    this.initSocketGetAgentPlatform(
      context.sharedDataContext.currentUser.userid,
      receptor
    );
    //console.log('context',context)
    document.addEventListener("mousedown", this.handelClick, false);
  }

  componentWillUnmount() {
    document.addEventListener("mousedown", this.handelClick, false);
  }

  handelClick = e => {
    if (this.node && this.node.contains(e.target)) {
      return;
    }
    this.setState({
      assegneeModalOpen: false
    });
  };

  componentWillReceiveProps = nextProps => {
    if (nextProps === undefined) {
      return false;
    } else {
      const context = this.context;
      const receptor = {
        headers: {
          "Content-Type": "application/json",
          publickey: context.sharedDataContext.currentUser.publickey,
          apisid: context.sharedDataContext.currentUser.apisid,
          sessionid: context.sharedDataContext.currentUser.session_id
        }
      };
      //  console.log("componentWillReceiveProps");
      //  console.log("this.state.ticketId",this.state.ticketId);
      // //this.initSocketgetAllTicket(receptor);
      if (this.state.ticketId === "") {
        this.setState(
          {
            ticketId: nextProps.ticketId
          },
          () => {
            this.initSocketTicketDetails(this.state.ticketId, receptor);
          }
        );
      }
    }
  };

  calculateUpdateTime = time => {
    const { lang } = this.state;
    let now = moment();
    let updated = moment(time);
    const days = now.diff(updated, "days");
    const hours = now.subtract(days, "days").diff(updated, "hours");
    const minutes = now.subtract(hours, "hours").diff(updated, "minutes");
    return `${
      days !== 0
        ? lang === "en"
          ? `${days} days ago`
          : `Il y a ${days} jour(s)`
        : hours === 0 || hours === 1
        ? lang === "en"
          ? `${minutes} min ago`
          : `Il y a ${minutes} minute(s)`
        : lang === "en"
        ? `${hours} hours ago`
        : `Il y a ${hours} heure(s)`
    }`;
  };
  //Star Get Agent Ticket
  onSocketGetAgentPlatform = response => {
    //console.log("onSocketGetAgentPlatformfff: ", response);
    if (response && (response.status === 200 || response.status === 202)) {
      // console.log("onSocketGetAgentPlatform : ", response.data);
      this.setState({ initAgents: response.data });
      this.setState({ storeInitAgents: response.data });
    }
  };

  initSocketGetAgentPlatform = (userid, receptor) => {
    // console.log("initSocketGetAgentPlatform ** : ");
    const { socket } = this.props;
    socket.on(SIO_AGENT_PLATFORM, response => {
      //   console.log("SIO_AGENT_PLATFORM : ", response);
      this.onSocketGetAgentPlatform(response);
    });

    CreateTicketHttpService.getAgentPlatFrom(userid, receptor).then(
      response => {
        //  console.log("getAgentPlatFrom : ", response);

        if (response.status === 200 || response.status === 202) {
          // console.log("test success : ", response);
        }
      }
    );
  };

  /* START GET TICKETS SETTINGS $$$$$$$$$$$$$$$$$$$$$$$$$$$$$ */
  // onSocketGetTicketDetails = response => {
  //   socket.on(SIO_GET_TICKET_DETAILS, response => {
  //     //console.log("SIO_GET_TICKET8DETAILS : ", response);
  //   });
  // };

  initSocketTicketDetails = (ticketId, receptor) => {
    // console.log("initSocketTicketDetails ");
    const { socket } = this.props;

    socket.on(SIO_GET_TICKET_DETAILS, response => {
      //console.log("SIO_TICKET_DETAILS : ", response);
      //  this.onSocketGetTicketDetails(response);
      this.setState({
        ticket: response.data,
        ticketPriority: {
          value: response.data.priority.name,
          label: response.data.priority.name
        },
        ticketStatus: {
          value: response.data.status.name,
          label: response.data.status.name
        },
        fileTicket: response.data.files
      });
    });

    GetAllTicketsHttpService.getTicketDetails(ticketId, receptor).then(
      response => {
        //console.log("getDatasTicketDetails : ", response);

        if (response.status === 200 || response.status === 202) {
          // console.log('test success : ', response);
        }
      }
    );
  };
  // add agent
  handleAddAgent = userId => {
    const { addAgent } = this.state;
    const updatedAgents = this.state.initAgents;
    const isExist = this.state.initAgents.findIndex(
      agent => agent.user_id === userId
    );
    for (let agent of updatedAgents) {
      agent.checked = false;
    }
    if (isExist !== -1) {
      updatedAgents[isExist].checked = true;
      this.setState(
        { addAgent: [], userData: { ...this.state.userData, checked: false } },
        () => {
          const add = this.state.initAgents.find(
            agent => agent.user_id === userId
          );
          this.setState({ addAgent: [add] });
        }
      );

      // addAgent.push(add);
      // this.setState({ addAgent });
    }
    this.setState({ initAgents: updatedAgents });
  };

  //remove agent
  handleRemoveAgent = userId => {
    const updatedAgents = this.state.initAgents;
    const isExist = this.state.initAgents.findIndex(
      agent => agent.user_id === userId
    );

    if (isExist !== -1) {
      updatedAgents[isExist].checked = false;
      const removeAgent = this.state.addAgent.filter(
        agent => agent.user_id !== userId
      );
      this.setState({ addAgent: removeAgent });
    }
    this.setState({ initAgents: updatedAgents });
  };

  // add My self
  handleAddMe = user => {
    const updatedAgents = this.state.initAgents;
    //const { addAgent } = this.state;
    const isExist = this.state.addAgent.findIndex(
      agent => agent.user_id === user.user_id
    );

    if (isExist === -1) {
      const { userData, addAgent } = this.state;
      userData.checked = true;
      for (let agent of updatedAgents) {
        agent.checked = false;
      }
      this.setState({ userData, addAgent: [] }, () => {
        this.setState({ addAgent: [userData] });
      });

      //  addAgent.push(userData);
    }
  };

  //handle remove me
  handleRemoveMe = user => {
    const isExist = this.state.addAgent.findIndex(
      agent => agent.user_id === user.user_id
    );

    if (isExist !== -1) {
      const { userData } = this.state;
      userData.checked = false;
      this.setState({ userData });

      const removeAgent = this.state.addAgent.filter(
        agent => agent.user_id !== user.user_id
      );
      this.setState({ addAgent: removeAgent });
    }
  };

  //search Agent
  handleInputSearchChange = event => {
    const { value } = event.currentTarget;
    this.setState({ searchAgent: value });

    const contentSearch = value;

    if (contentSearch !== "") {
      this.setState(prevState => ({
        ...prevState,
        initAgents: this.state.storeInitAgents.filter(
          option =>
            option.firstname
              .toLowerCase()
              .includes(contentSearch.toLowerCase()) ||
            option.lastname.toLowerCase().includes(contentSearch.toLowerCase())
        )
      }));
    } else {
      this.setState(prevState => ({
        ...prevState,
        initAgents: this.state.storeInitAgents
      }));
    }
  };

  // change ticket priotity
  handleOnChangePrio = value => {
    this.setState({ ticketPriority: value }, () =>
      this.handleSubmitUpdateTicket(1)
    );
  };
  // change ticket status
  handleOnChangeStat = value => {
    // console.log("value", value);
    this.setState({ ticketStatus: value }, () =>
      this.handleSubmitUpdateTicket(2)
    );
  };

  // Update Ticket
  handleSubmitUpdateTicket(state) {
    let prio = {};
    const { addAgent, ticketPriority, ticketStatus } = this.state;
    if (state === 0 && addAgent.length !== 0) {
      this.buildDataUpdateTicket(this.state.addAgent[0], state);
      this.initSocketUpdateTicket();
    }
    if (state === 1) {
      if (ticketPriority.value === "High") {
        prio.name = ticketPriority.value;
        prio.label = "Very urgent";
      } else if (ticketPriority.value === "Medium") {
        prio.name = ticketPriority.value;
        prio.label = "Urgent";
      } else {
        prio.name = ticketPriority.value;
        prio.label = "Not urgent";
      }
      this.buildDataUpdateTicket(prio, state);
      this.initSocketUpdateTicket();
    }
    if (state === 2) {
      if (ticketStatus.value === "Resolved") {
        prio.name = ticketStatus.value;
        prio.label = "Ticket resolved & close";
      } else if (ticketStatus.value === "Pending") {
        prio.name = ticketStatus.value;
        prio.label = "On-going ticket";
      } else {
        prio.name = ticketStatus.value;
        prio.label = "Ticket without agent assign";
      }

      this.buildDataUpdateTicket(prio, state);
      this.initSocketUpdateTicket();
    }
  }
  // build Data Update ticket
  buildDataUpdateTicket = (dataToUpdate, state) => {
    // console.log('dataToUpdate :',dataToUpdate,' ;state :',state)
    const updateTicket = {
      sio_channel: SIO_TICKET_UPDATE,
      updated_by: {
        user_id: this.state.userData.userid,
        firstname: this.state.userData.firstname,
        lastname: this.state.userData.lastname,
        email: this.state.userData.email,
        phone: this.state.userData.phone
      },
      type:
        state === 0 ? "assigned_agent" : state === 1 ? "priority" : "status",
      value: dataToUpdate
    };

    if (localStorage && updateTicket) {
      localStorage.setItem(
        "sv_tmp_update_ticket",
        JSON.stringify(updateTicket)
      );
    }
  };
  //InitSocket Update Ticket

  initSocketUpdateTicket = () => {
    const context = this.context;
    const receptor = {
      headers: {
        "Content-Type": "application/json",
        publickey: context.sharedDataContext.currentUser.publickey,
        apisid: context.sharedDataContext.currentUser.apisid,
        sessionid: context.sharedDataContext.currentUser.session_id
      }
    };
    const { socket } = this.props;
    // console.log("initSocketUpdateTicket : ", context);
    socket.on(SIO_TICKET_UPDATE, response => {
      // console.log("initSocketUpdateTicket : ", response);
      this.props.handleMessageTicket(
        "successUpdate",
        response.data,
        response.data.id
      );
      this.initSocketTicketDetails(response.data.id, receptor);
      this.onSocketUpdateTicket(response);
    });
    this.handleUpdateTicketSubmit(this.state.ticket.id);
  };
  //onsocket Update Ticket
  onSocketUpdateTicket = response => {
    if (response && response.status === 200) {
      //console.log("onSocketUpdateTicket")

      this.setState({
        assegneeModalOpen: false
      });
    }
  };
  //handleUpdateTicketSubmit
  handleUpdateTicketSubmit = id => {
    const context = this.context;
    const receptor = {
      headers: {
        "Content-Type": "application/json",
        publickey: context.sharedDataContext.currentUser.publickey,
        apisid: context.sharedDataContext.currentUser.apisid,
        sessionid: context.sharedDataContext.currentUser.session_id
      }
    };

    const dataUpdateTicket = JSON.parse(
      localStorage.getItem("sv_tmp_update_ticket")
    );

    UpdateTicketHttpService.updateTicket(id, dataUpdateTicket, receptor)
      .then(response => {
        //   console.log("UpdateTicketHttpService : ", response);

        if (response && response.data && response.data.status === 202) {
          // this.setState({ dataInputTicket: [] });
          // this.setState({ objetTicket: "" });
          // this.setState({ messageTicket: EditorState.createEmpty() });

          localStorage.removeItem("sv_tmp_update_ticket");
        } else {
          // eslint-disable-next-line react/destructuring-assignment
          this.props.handleMessageTicket("error", "--", "--");
        }
      })
      .catch(error => {
        // console.log("**** print error ****", error);

        // eslint-disable-next-line react/destructuring-assignment
        this.props.handleMessageTicket("error", "--", "--");
      });
  };

  render() {
    const { i18n, t, kind, handleCreateTicket } = this.props;

    const {
      displayMessage,
      status,
      priority,
      assegneeModalOpen,
      ticket,
      ticketPriority,
      ticketStatus,
      addAgent,
      fileTicket,
      lang
    } = this.state;

    // console.log("render ticketId", this.state.ticketId);
    // console.log(" render ticket", ticket);
    // console.log("ticketPriority", ticketPriority);

    const statusView = status.map((elm, i) => ({
      ...elm,
      label: (
        <>
          <button
            className="ticket-select-status"
            style={{
              background: `${
                elm.index === "Pending" || elm.index === "en attente"
                  ? "#FF9B21"
                  : elm.index === "New" || elm.index === "Nouveau"
                  ? "#0089E1"
                  : "#00BD39"
              }`
            }}
          />
          {elm.label}
        </>
      )
    }));

    const priorutyView = priority.map((elm, i) => ({
      ...elm,
      label: (
        <p
          className="ticket-select-priority"
          style={{
            background: `${
              elm.index === "High"
                ? "#eb592321"
                : elm.index === "Low"
                ? "#6572884a"
                : "#ff9b214d"
            }`,
            color: `${
              elm.index === "High"
                ? "#EB5923"
                : elm.index === "Low"
                ? "#657288"
                : "#FF9B21"
            }`
          }}
        >
          {elm.label}
        </p>
      )
    }));

    const customStyles = {
      option: (provided, state) => ({
        ...provided,
        backgroundColor: state.isSelected ? "#ddd" : "#fff",
        color: state.isSelected ? "#000" : "#000",
        cursor: "pointer !important"
      })
    };
    const ticketStatusa = {
      value: ticketStatus.label,
      label:
        ticketStatus.label === "Resolved" || ticketStatus.label === "Résolu"
          ? t("settings.tickets_status_content.resolved")
          : ticketStatus.label === "Pending" ||
            ticketStatus.label === "en attente"
          ? t("settings.tickets_status_content.pending")
          : t("settings.tickets_status_content.new")
    };
    // console.log('ticketStatusa',ticketStatusa)
    const ticketPrioritya = {
      value: ticketPriority.label,
      label:
        ticketPriority.label === "High" || ticketPriority.label === "Haute"
          ? t("settings.tickets_priority_content.hight")
          : ticketPriority.label === "Low" || ticketPriority.label === "Faible"
          ? t("settings.tickets_priority_content.low")
          : t("settings.tickets_priority_content.medium")
    };

    //GET CUSTOMER INFO
    const customerEmail = ticket.customer_information
      ? ticket.customer_information
          .map((elm, i) => {
            if (elm !== null && i > 1 && elm.type === "email") {
              return elm.value;
            }
          })
          .join(" ")
      : false;

    const customerPhone = ticket.customer_information
      ? ticket.customer_information
          .map((elm, i) => {
            if (elm !== null && i > 1 && elm.name === "Telephone") {
              return elm.value;
            }
          })
          .join(" ")
      : false;

    const customerName = ticket.customer_information
      ? ticket.customer_information
          .map((elm, i) => {
            if (elm !== null && i < 2) {
              return elm.value;
            }
          })
          .join(" ")
      : false;

    //console.log("customerInfo", ticket.customer_information);
    return (
      <>
        <div className="header-indicator">
          <h3 className="header-indic-title1">
            {t("tickets.details_ticket.header.ticket_table")}
          </h3>
          {" > "}
          <p className="header-indic-title2">
            {t("tickets.details_ticket.header.tickets_message")}
          </p>
        </div>
        <div className="ticketnalytics-header">
          <h2 className="dashboard-title">
            {t("tickets.details_ticket.ticket_number")} #{ticket.number}
          </h2>
          <button
            className="create_ticketbtn"
            onClick={() => handleCreateTicket()}
          >
            + {t("tickets.ticket_btn_create").toUpperCase()}
          </button>
        </div>
        <div className="columns analytics-columns MessageTicket-conaitner">
          <div className="section1 tecket-detail">
            <div className="profilePicture-contain">
              <img src={ProfileIcon} className="profilepicture" />
            </div>
            <div className="profil-contain">
              <h3 className="profil-name">
                {ticket && ticket.created_by
                  ? ticket.created_by.first_name +
                    " " +
                    ticket.created_by.last_name
                  : false}
              </h3>
              <p className="creationDate">{ticket.created_at}</p>
              <h1 className="ticketTitle">{ticket.subject}</h1>
              <div className="dashet-line" />
              <h3 className="customer-text text-blue">
                {t("tickets.details_ticket.customer_details")}
              </h3>
              <div className="massageticket-input-cont">
                <div className="display-user-info" data-tooltip={customerName}>
                  <img
                    src={User}
                    className="massageticket-log"
                    alt="icon user"
                  />
                  <span className="custumers-detail">
                    {customerName
                      ? customerName.length > 23
                        ? customerName.substring(0, 20) + "..."
                        : customerName
                      : ""}
                  </span>
                </div>
                <div
                  className="display-user-info"
                  data-tooltip={customerEmail ? customerEmail : ""}
                >
                  <img
                    src={Envelope}
                    className="massageticket-log"
                    alt="icon mail"
                  />{" "}
                  <span className="custumers-detail">
                    {" "}
                    {/* {customerEmail===undefined ? '' : customerEmail} */}
                    {customerEmail
                      ? customerEmail.length > 23
                        ? customerEmail.substring(0, 20) + "..."
                        : customerEmail
                      : ""}
                  </span>
                </div>
                <div
                  className="display-user-info"
                  data-tooltip={customerPhone ? customerPhone : ""}
                >
                  <img
                    src={Phone}
                    className="massageticket-log"
                    alt="icon phone"
                  />{" "}
                  <span className="custumers-detail">
                    {customerPhone
                      ? customerPhone.length > 23
                        ? customerPhone.substring(0, 20) + "..."
                        : customerPhone
                      : ""}
                  </span>
                </div>
              </div>
              <div
                style={{
                  marginTop: "30px",
                  display: "flex",
                  alignItems: "center"
                }}
              >
                <div
                  style={{
                    borderTop: "1px dashed #c8d3d6",
                    width: "100%",
                    height: "1px"
                  }}
                />
                <span className="messagedisplay-btn">
                  <img
                    src={ArrowDown}
                    style={{
                      height: "10px",
                      width: "19px"
                    }}
                    onClick={() =>
                      this.setState({ displayMessage: !displayMessage })
                    }
                  />
                </span>
                <div
                  style={{
                    borderTop: "1px dashed #c8d3d6",
                    width: "100%",
                    height: "1px"
                  }}
                />
              </div>
              <div style={{ height: "200px" }}>
                <div
                  className="massageticket-container"
                  style={{
                    display: displayMessage ? "block" : "none",
                    height: "100%"
                  }}
                >
                  <p>
                    {ticket.message && typeof ticket.message == "string"
                      ? updateMessage2(ticket.message)
                      : ""}
                  </p>
                </div>
              </div>
              {/* <div className="dashet-line" /> */}
              <h3
                className="customer-text"
                style={{
                  color: "#4C4C4C",
                  marginLeft: "0",
                  marginBottum: "20px"
                }}
              >
                {fileTicket.length > 0 && fileTicket[0] !== null
                  ? `${fileTicket.length} ${t(
                      "tickets.details_ticket.attachement"
                    )}`
                  : false}
              </h3>
              <div className="File-Uplaoded-container">
                {fileTicket[0] !== null
                  ? fileTicket.map((file, i) => {
                      return (
                        <p
                          onClick={() => logout(file)}
                          style={{
                            cursor: "pointer",
                            textDecoration: "underline",
                            display: "flex",
                            alignItems: "center",
                            padding: "10px",
                            flexDirection: "column"
                          }}
                        >
                          <img
                            src={DownloadIcon}
                            alt="dwonload Icon"
                            style={{ maxWidth: "55%", marginRight: "7px" }}
                          />
                          <p>File {i + 1} </p>
                        </p>
                      );
                    })
                  : false}
                {fileTicket.length > 0 && fileTicket[0] !== null ? (
                  <span className=" assign-agent-btn">+</span>
                ) : (
                  false
                )}
              </div>
            </div>
          </div>
          <div className="section2">
            <div className="ticketAgent-container">
              <span className="ticketkeys">
                {t("tickets.details_ticket.date")}
              </span>{" "}
              <p className="ticketinfos displayInput">{ticket.created_at}</p>
            </div>
            <div className="ticketAgent-container">
              <span className="ticketkeys">
                {t("tickets.details_ticket.created")}
              </span>{" "}
              <p className="ticketinfos displayInput">
                {" "}
                <img
                  src={ProfileIcon}
                  className="profilepicture-Created"
                />{" "}
                {ticket && ticket.created_by
                  ? ticket.created_by.first_name +
                    " " +
                    ticket.created_by.last_name
                  : false}
              </p>
            </div>
            <div className="ticketAgent-container">
              <span className="ticketkeys ">
                {t("tickets.details_ticket.assignee")}
              </span>{" "}
              <p className="ticketinfos display-user-assegne">
                <img src={ProfileIcon} className="profilepicture-assignee" />
                <span className="custumers-detail">
                  {" "}
                  {ticket &&
                  ticket.assigned_agent &&
                  Object.keys(ticket.assigned_agent).length !== 0
                    ? ticket.assigned_agent.firstname +
                      " " +
                      ticket.assigned_agent.lastname
                    : ""}
                </span>
              </p>{" "}
              <div className="assign-agent-Container">
                {ticketStatus.value === "Résolue" ||
                ticketStatus.value === "Resolved" ? (
                  ""
                ) : (
                  <p
                    className="resseign-messageTick"
                    onClick={() =>
                      this.setState({
                        assegneeModalOpen: !this.state.assegneeModalOpen
                      })
                    }
                    ref={node => (this.node = node)}
                  >
                    {t("tickets.details_ticket.reassign")}
                  </p>
                )}
                {/* composant Assign Agent to Ticket: Boutton Reassign  */}
                <div
                  className="assign-text-modal"
                  style={{
                    display: `${assegneeModalOpen ? "flex" : "none"}`,
                    left: "-144px"
                  }}
                  ref={node => (this.node = node)}
                >
                  <h2 className="title assign-modal-title">
                    {t("tickets.create_ticket.assign_agent_ticket")}
                  </h2>
                  <ul className="menu-list menu-list-ticket">
                    {" "}
                    <li className="assign-self">
                      <img src={ProfileIcon} alt="portrait" />
                      <span className="user-name">
                        {t("tickets.create_ticket.assign_ticket_myself")}
                      </span>
                      {this.state.userData.checked ||
                      this.state.userData.checked === true ? (
                        <span
                          className="remove-user"
                          onClick={e =>
                            this.handleRemoveMe(this.state.userData)
                          }
                        >
                          -
                        </span>
                      ) : (
                        <span
                          className="add-user"
                          onClick={e => this.handleAddMe(this.state.userData)}
                        >
                          +
                        </span>
                      )}
                    </li>
                  </ul>
                  <div className="search-box assign-search">
                    <input
                      className="input"
                      type="text"
                      placeholder={t(
                        "tickets.create_ticket.search_agent_input"
                      )}
                      value={this.state.searchAgent}
                      onChange={e => this.handleInputSearchChange(e)}
                    />
                    <img src={SearchIcon} alt="search" />
                  </div>
                  <section
                    className="modal-card-body"
                    style={{ width: "100%" }}
                  >
                    {/*  Composant list add Agent Create Ticket */}
                    <ul className=" menu-list menu-list-ticket">
                      {this.state.initAgents &&
                        ticket.assigned_agent &&
                        this.state.initAgents.map((item, i) => {
                          if (
                            ticket.assigned_agent.firstname !==
                              item.firstname &&
                            ticket.assigned_agent.lastname !== item.lastname
                          ) {
                            return (
                              <li key={i}>
                                <img src={ProfileIcon} alt="portrait" />
                                <span className="user-name">
                                  {item.firstname} {item.lastname}
                                </span>
                                {item.checked || item.checked === true ? (
                                  <span
                                    className="remove-user"
                                    onClick={e =>
                                      this.handleRemoveAgent(item.user_id)
                                    }
                                  >
                                    -
                                  </span>
                                ) : (
                                  <span
                                    className="add-user"
                                    onClick={e =>
                                      this.handleAddAgent(item.user_id)
                                    }
                                  >
                                    +
                                  </span>
                                )}
                              </li>
                            );
                          }
                        })}
                    </ul>
                    {/*  */}
                  </section>
                  <footer className="assign-modal-footer">
                    <button
                      className="button is-primary"
                      aria-label="close"
                      style={{ width: "100%" }}
                      onClick={() => this.handleSubmitUpdateTicket(0)}
                    >
                      {t("tickets.create_ticket.Assign")}
                    </button>
                  </footer>

                  {/* End Modal add agnet */}
                </div>

                {/* ** ** ** */}
              </div>
            </div>
            <div className="ticketAgent-container ticketprio">
              <span className="ticketkeys">
                {t("tickets.details_ticket.priority")}
              </span>{" "}
              <Select
                styles={customStyles}
                theme={theme => ({
                  ...theme,
                  colors: {
                    ...theme.colors,
                    primary: "#eee",
                    primary25: "#eee"
                  }
                })}
                options={priorutyView}
                onChange={this.handleOnChangePrio}
                value={ticketPrioritya}
                isSearchable={false}
                className="ticket-Select"
                placeholder=""
                isDisabled={
                  ticketStatus.value === "Résolue" ||
                  ticketStatus.value === "Resolved"
                    ? true
                    : false
                }
              />
              {/* <p className="ticketinfos">Adisa Kola</p> */}
            </div>
            <div
              className="ticketAgent-container ticketStatus"
              style={{ zIndex: "0" }}
            >
              <span className="ticketkeys">
                {t("tickets.details_ticket.status")}
              </span>{" "}
              <Select
                styles={customStyles}
                options={statusView}
                onChange={this.handleOnChangeStat}
                value={ticketStatusa}
                isSearchable={false}
                placeholder=""
                isDisabled={
                  ticketStatus.value === "Résolue" ||
                  ticketStatus.value === "Resolved"
                    ? true
                    : false
                }
              />
            </div>
            <div className="ticket-log-container">
              <div className="ticket-log-title">
                {t("tickets.details_ticket.ticket_log_activities")}
              </div>

              {ticket && ticket.logs
                ? ticket.logs.map((ticket, i) =>
                    i < 5 ? (
                      <div
                        key={i}
                        className={`ticket-log ${
                          i === ticketsLog.length - 1 ? "last" : ""
                        }`}
                      >
                        {/* <img src={} alt="profile picture"/> */}

                        <img src={ProfileIcon} className="profilepicture-log" />

                        <div className="ticket-activities">
                          <p
                            className="activity"
                            style={{ fontSize: "12.5px" }}
                          >
                            {ticket.message_en && ticket.message_fr
                              ? lang === "en"
                                ? updateMessage(ticket.message_en)
                                : updateMessage(ticket.message_fr)
                              : updateMessage(ticket.message)}
                          </p>
                          <p className="activity-date ">
                            {this.calculateUpdateTime(ticket.created_at)}
                          </p>
                        </div>
                      </div>
                    ) : (
                      false
                    )
                  )
                : false}
            </div>
            {ticketStatus.value === "Résolue" ||
            ticketStatus.value === "Resolved" ? (
              false
            ) : (
              <button
                className="clode_ticketbtn"
                onClick={() =>
                  this.handleOnChangeStat({
                    value: "Resolved",
                    index: "Resolved"
                  })
                }
              >
                {t("tickets.details_ticket.close_ticket")}
              </button>
            )}
          </div>
        </div>
        <Footer t={t} />
      </>
    );
  }
}

MessageTicket.propTypes = {
  i18n: PropTypes.shape({}).isRequired,
  t: PropTypes.func.isRequired,
  kind: PropTypes.string.isRequired,
  handleCreateTicket: PropTypes.func.isRequired
};

const MessageTicketWithSocket = props => (
  <DataSocketContext.Consumer>
    {socket => <MessageTicket {...props} socket={socket} />}
  </DataSocketContext.Consumer>
);

export default MessageTicketWithSocket;
