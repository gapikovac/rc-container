/* eslint-disable react/no-unused-state */
import React, { Component, useContext } from "react";
import PropTypes from "prop-types";

// Use Socket io - import
//import io from "socket.io-client";

import { Formik, Form, Field } from "formik";
import Upload from "./upload/Upload";
//import FileUploadProgress from "react-fileupload-progress";

import { Editor } from "react-draft-wysiwyg";
import { EditorState, convertToRaw } from "draft-js";
import draftToHtml from "draftjs-to-html";
// import htmlToDraft from 'html-to-draftjs';
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

import Select from "react-select";

import { SharedDataContext, DataSocketContext } from "../../app/UseContext";
import Progress from "./progress/Progress";
import SearchIcon from "../../../assets/images/profile/search.svg";
import ProfileIcon from "../../../assets/images/profile/user.svg";
import Footer from "../../layouts/Footer";
/* START $$$$$$$$$$$$$$$$$$$$$$$$$$$$$ */
import {
  TicketSettingsHttpService,
  CreateTicketHttpService
} from "../../../services/HttpService";
import {
  SOCKET,
  SIO_TICKET_SETTINGS,
  SIO_CREATE_TICKET,
  REGEX_EMAIL,
  REGEX_TEXT,
  REGEX_NUMBER,
  REGEX_DATE,
  SIO_AGENT_PLATFORM
} from "../../../constants/Constants";
import { CONSTANT } from "../../../constants/browser";

//const socket = io(SOCKET.BASE_URL);
/* END $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ */

function replaceMessage(retStr) {
  if (Object.keys(retStr)[0] === "_immutable") {
    return false;
  }
  String.prototype.allReplace = function(obj) {
    var retStr = this;
    for (var x in obj) {
      retStr = retStr.replace(new RegExp(x, "g"), obj[x]);
    }
    return retStr;
  };
  return retStr.allReplace({
    "<p>": "",
    "</p>": "",
    "&nbsp;": "",
    "&nbs;": ""
  });
}

//valid Phone Number
function testingPhone(str) {
  return !REGEX_NUMBER.test(str)
    ? str.replace(str.charAt(str.length - 1), "")
    : str;
  //return str ? str.match(REGEX_NUMBER) ? str : '':''
  // console.log(str)
}

//console.log(str.allReplace({'<p>': '', '</p>': '', '&nbsp;': '', '&nbs;': ''}));
class CreateTicket extends Component {
  // eslint-disable-next-line react/state-in-constructor
  state = {
    lang: "",
    ticketLang: "",
    ticketOnCreation: {},
    assegneeModalOpen: false,
    initAgents: [],
    storeInitAgents: [],
    fileToUpload: [],
    uploadedFile: [],
    uploading: false,
    uploadProgress: {},
    successfullUploaded: true,
    ticketSettingsInput: [],
    prioritySetting: [],
    ticketSettingsInputActive: true,
    prioritySettingActive: true,
    allFileUploadedsuccess: false,
    dataInputTicket: [],
    objetTicket: "",
    priorityTicket: {},
    messageTicket: EditorState.createEmpty(),

    errorValidator: [],
    checkError: false,
    addAgent: [],
    userData: {},
    validCreateTicket: false,
    searchAgent: ""
  };
  static contextType = SharedDataContext;

  componentDidMount() {
    const context = this.context;
    const receptor = {
      headers: {
        "Content-Type": "application/json",
        publickey: context.sharedDataContext.currentUser.publickey,
        apisid: context.sharedDataContext.currentUser.apisid,
        sessionid: context.sharedDataContext.currentUser.session_id
      }
    };

    this.initSocketTicketSettings(receptor);
    this.initSocketGetAgentPlatform(
      context.sharedDataContext.currentUser.userid,
      receptor
    );
    this.setState({
      userData: { ...context.sharedDataContext.currentUser, checked: false },
      lang: context.sharedDataContext.defaultLang.value
    });
    document.addEventListener("mousedown", this.handelClick, false);
  }

  componentWillUnmount() {
    document.addEventListener("mousedown", this.handelClick, false);
  }

  //close modam anywhere you click
  handelClick = e => {
    if (this.node && this.node.contains(e.target)) {
      return;
    }
    this.setState({
      assegneeModalOpen: false
    });
  };

  /* START GET TICKETS SETTINGS $$$$$$$$$$$$$$$$$$$$$$$$$$$$$ */
  //onSocketGetTicketSettings = response => {};

  initSocketTicketSettings = receptor => {
    // TODO reactivate later
    /*const { socket } = this.props;
    socket.on(SIO_TICKET_SETTINGS, response => {
      //console.log("SIO_TICKET_SETTINGS : ", response);
      // this.onSocketGetTicketSettings(response);
      const { lang } = this.state;
      if (response && (response.status === 200 || response.status === 202)) {
        let responseDataPriority = [];
        if (lang === "en") {
          this.setState({
            ticketSettingsInput:
              response.data[0].lang_en.customer_information.items,
            ticketSettingsInputActive:
              response.data[0].lang_en.customer_information.active,
            //
            prioritySettingActive: response.data[0].lang_en.priority.active
          });
          responseDataPriority = response.data[0].lang_en.priority.items;
        }
        if (lang === "fr") {
          this.setState({
            ticketSettingsInput:
              response.data[0].lang_fr.customer_information.items,
            ticketSettingsInputActive:
              response.data[0].lang_fr.customer_information.active,
            //
            prioritySettingActive: response.data[0].lang_fr.priority.active
          });
          responseDataPriority = response.data[0].lang_fr.priority.items;
        }

        // mise à true, car pas de champ Input
        if (!this.state.ticketSettingsInputActive) {
          this.setState({ validCreateTicket: true });
        }

        // refact datas priority
        const refactPriority = [];
        responseDataPriority.map(item => {
          refactPriority.push({ value: item.name, label: item.name });
        });
        this.setState({ prioritySetting: refactPriority });
      }
    });*/

    TicketSettingsHttpService.getDatasTicketSettings(receptor).then(
      response => {
        // console.log('getDatasTicketSettings : ', response);

        if (response.status === 200 || response.status === 202) {
          const { lang } = this.state;
          if (
            response &&
            (response.status === 200 || response.status === 202)
          ) {
            let responseDataPriority = [];
            if (lang === "en") {
              this.setState({
                ticketSettingsInput:
                  response.data[0].lang_en.customer_information.items,
                ticketSettingsInputActive:
                  response.data[0].lang_en.customer_information.active,
                //
                prioritySettingActive: response.data[0].lang_en.priority.active
              });
              responseDataPriority = response.data[0].lang_en.priority.items;
            }
            if (lang === "fr") {
              this.setState({
                ticketSettingsInput:
                  response.data[0].lang_fr.customer_information.items,
                ticketSettingsInputActive:
                  response.data[0].lang_fr.customer_information.active,
                //
                prioritySettingActive: response.data[0].lang_fr.priority.active
              });
              responseDataPriority = response.data[0].lang_fr.priority.items;
            }

            /** mise à true, car pas de champ Input */
            if (!this.state.ticketSettingsInputActive) {
              this.setState({ validCreateTicket: true });
            }

            // refact datas priority
            const refactPriority = [];
            responseDataPriority.map(item => {
              refactPriority.push({ value: item.name, label: item.name });
            });
            this.setState({ prioritySetting: refactPriority });
          }
        }
      }
    );
  };
  /* END $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ */

  /* START GET AGENT PLATFORM $$$$$$$$$$$$$$$$$$$$$$$$$$$$$ */
  // onSocketGetAgentPlatform = response => {
  //   //   console.log("onSocketGetAgentPlatformfff: ", response);
  //   if (response && (response.status === 200 || response.status === 202)) {
  //     //   console.log("onSocketGetAgentPlatform : ", response.data);

  //   }
  // };

  initSocketGetAgentPlatform = (userid, receptor) => {
    /*const { socket } = this.props;
    //  console.log("initSocketGetAgentPlatform ** : ");

    socket.on(SIO_AGENT_PLATFORM, response => {
      //   console.log("SIO_AGENT_PLATFORM : ", response);
      // this.onSocketGetAgentPlatform(response);
      this.setState({ initAgents: response.data });
      this.setState({ storeInitAgents: response.data });
    });*/

    CreateTicketHttpService.getAgentPlatFrom(userid, receptor).then(
      response => {
        //     console.log("getAgentPlatFrom : ", response);

        if (response.status === 200 || response.status === 202) {
          this.setState({ initAgents: response.data });
          this.setState({ storeInitAgents: response.data });
        }
      }
    );
  };
  /* END $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ */

  // eslint-disable-next-line react/sort-comp
  changeAvatar = event => {
    const image = event.target.files[0];
    // this.uploadPhoto(image);
    const fd = new FormData();

    fd.append("image", image);
    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    };
    // return post(URL, fd, config).then(res => {
    //   let path = JSON.parse(res.data).file_path;
    //   this.setState({
    //     profile: path
    //   });
    // });
  };

  handleAddAgent = userId => {
    const { addAgent } = this.state;
    const updatedAgents = this.state.initAgents;
    const isExist = this.state.initAgents.findIndex(
      agent => agent.user_id === userId
    );
    for (let agent of updatedAgents) {
      agent.checked = false;
    }
    if (isExist !== -1) {
      updatedAgents[isExist].checked = true;
      this.setState(
        { addAgent: [], userData: { ...this.state.userData, checked: false } },
        () => {
          const add = this.state.initAgents.find(
            agent => agent.user_id === userId
          );
          this.setState({ addAgent: [add] });
        }
      );

      // addAgent.push(add);
      // this.setState({ addAgent });
    }
    this.setState({ initAgents: updatedAgents });
  };

  handleRemoveAgent = userId => {
    const updatedAgents = this.state.initAgents;
    const isExist = this.state.initAgents.findIndex(
      agent => agent.user_id === userId
    );

    if (isExist !== -1) {
      updatedAgents[isExist].checked = false;
      const removeAgent = this.state.addAgent.filter(
        agent => agent.user_id !== userId
      );
      this.setState({ addAgent: removeAgent });
    }
    this.setState({ initAgents: updatedAgents });
  };

  handleAddMe = user => {
    const updatedAgents = this.state.initAgents;
    const { addAgent } = this.state;
    const isExist = this.state.addAgent.findIndex(
      agent => agent.user_id === user.user_id
    );

    if (isExist === -1) {
      const { userData } = this.state;
      userData.checked = true;
      for (let agent of updatedAgents) {
        agent.checked = false;
      }
      this.setState({ userData, addAgent: [] }, () => {
        this.setState({ addAgent: [userData] });
      });

      //  addAgent.push(userData);
    }
  };

  handleRemoveMe = user => {
    const isExist = this.state.addAgent.findIndex(
      agent => agent.user_id === user.user_id
    );

    if (isExist !== -1) {
      const { userData } = this.state;
      userData.checked = false;
      this.setState({ userData });

      const removeAgent = this.state.addAgent.filter(
        agent => agent.user_id !== user.user_id
      );
      this.setState({ addAgent: removeAgent });
    }
  };

  handleInputChange = (event, item, type, i) => {
    const { dataInputTicket } = this.state;
    //  console.log("dataInputTicket", dataInputTicket);
    const { value } = event.currentTarget;
    dataInputTicket[i] = {
      type: item.type,
      name: item.name,
      value: value
    };
    this.setState({ dataInputTicket });
  };

  handleverification = i => {
    const { t } = this.props;
    const { dataInputTicket } = this.state;
    const { errorValidator } = this.state;
    const inputvalue = dataInputTicket[i];

    if (inputvalue !== undefined) {
      switch (dataInputTicket[i].type.toLowerCase()) {
        case "email":
          if (
            !REGEX_EMAIL.test(dataInputTicket[i].value) &&
            dataInputTicket[i].value !== ""
          ) {
            errorValidator[i] = {
              text: t("validation_Field_form.error_field_email")
            };
            this.setState({ ...errorValidator, errorValidator });
            this.setState({ validCreateTicket: false });
          } else {
            this.setState({
              errorValidator: errorValidator.map((elm, index) =>
                i === index ? null : elm
              )
            });
            this.setState({ validCreateTicket: true });
          }
          break;
        case "text":
          if (
            !REGEX_TEXT.test(dataInputTicket[i].value) &&
            dataInputTicket[i].value !== ""
          ) {
            errorValidator[i] = {
              text: t("validation_Field_form.error_field_text")
            };
            this.setState({ errorValidator });
            this.setState({ validCreateTicket: false });
          } else {
            this.setState({
              errorValidator: errorValidator.map((elm, index) =>
                i === index ? null : elm
              )
            });
            this.setState({ validCreateTicket: true });
          }
          break;
        case "number":
          if (
            (dataInputTicket[i].value !== "" &&
              dataInputTicket[i].value.length < 1) ||
            (dataInputTicket[i].value.length > 0 &&
              !REGEX_NUMBER.test(dataInputTicket[i].value))
          ) {
            errorValidator[i] = {
              text: t("validation_Field_form.error_filed_number")
            };
            this.setState({ errorValidator });
            this.setState({ validCreateTicket: false });
          } else {
            this.setState({
              errorValidator: errorValidator.map((elm, index) =>
                i === index ? null : elm
              )
            });
            this.setState({ validCreateTicket: true });
          }
          break;
        case "date":
          if (
            !REGEX_DATE.test(dataInputTicket[i].value) &&
            dataInputTicket[i].value !== ""
          ) {
            errorValidator[i] = {
              text: t("validation_Field_form.error_field_date")
            };
            this.setState({ errorValidator });
            this.setState({ validCreateTicket: false });
          } else {
            this.setState({
              errorValidator: errorValidator.map((elm, index) =>
                i === index ? null : elm
              )
            });
            this.setState({ validCreateTicket: true });
          }
          break;
        default:
          errorValidator[i] = {
            text: t("validation_Field_form.error_field_not_found")
          };
          this.setState({ errorValidator });
          this.setState({ validCreateTicket: false });
      }
    } else {
      return false;
    }
  };

  handleObjetChange = event => {
    const { value } = event.currentTarget;
    this.setState({ objetTicket: value });
  };

  handlePriorityChange = value => {
    this.setState({ priorityTicket: value });
  };

  onEditorStateChange = messageTicket => {
    const valueEditor = draftToHtml(
      convertToRaw(messageTicket.getCurrentContent())
    );
    this.setState({ messageTicket: valueEditor });
  };

  // eslint-disable-next-line react/sort-comp
  handleSubmitCreateTicket() {
    const {
      dataInputTicket,
      objetTicket,
      priorityTicket,
      messageTicket,
      fileToUpload,
      errorValidator,
      successfullUploaded
    } = this.state;

    if (!successfullUploaded)
      this.props.handleMessageTicket("error-file-upload-progress", "--", "--");
    else if (
      (this.state.ticketSettingsInputActive === true &&
        dataInputTicket.length === 0) ||
      (dataInputTicket.length >= 1 &&
        (dataInputTicket[0] === undefined ||
          dataInputTicket[0].value.trim() === ""))
    )
      this.props.handleMessageTicket("error-Customer-field-empty", "--", "--");
    else if (objetTicket.trim() === "")
      this.props.handleMessageTicket("error-Subject-field-empty", "--", "--");
    else if (Object.keys(priorityTicket).length === 0)
      this.props.handleMessageTicket("error-Priority-field-empty", "--", "--");
    else if (
      !replaceMessage(messageTicket) ||
      (replaceMessage(messageTicket) &&
        replaceMessage(messageTicket).charCodeAt(0) === 10)
    )
      this.props.handleMessageTicket("error-Message-field-empty", "--", "--");
    //els if(fileToUpload.length===0) this.props.handleMessageTicket("error-File-field-empty", "--", "--")
    else if (this.state.addAgent.length === 0)
      this.props.handleMessageTicket("error-Agent-field-empty", "--", "--");
    else if (errorValidator.join("") !== "") return false;
    else {
      this.buildDataCreateTicket(
        dataInputTicket,
        objetTicket,
        priorityTicket,
        messageTicket,
        fileToUpload
      );

      this.initSocketCreateTicket();
    }
  }

  /** Start - send customerFiled */
  buildDataCreateTicket = (
    dataInputTicket,
    objetTicket,
    priorityTicket,
    messageTicket,
    uploadedFile
  ) => {
    const prio = {
      name: priorityTicket.label,
      type: priorityTicket.label
    };
    const createTicket = {
      sio_channel: SIO_CREATE_TICKET,
      ticket_information: {
        subject: objetTicket,
        message: messageTicket,
        files: uploadedFile,
        priority: prio,
        category: {
          label: "Technical" // Technical, Customer care, Enquires
        },
        customer_information: dataInputTicket,
        status: {
          name: "New",
          type: "Ticket without agent assign"
        },
        assigned_agent:
          this.state.addAgent.length > 0 ? this.state.addAgent[0] : {},
        created_by: {
          agent_id: this.state.userData.userid,
          first_name: this.state.userData.firstname,
          last_name: this.state.userData.lastname,
          email: this.state.userData.email,
          telephone: this.state.userData.phone
        },
        closed: true
      }
    };

    if (localStorage && createTicket) {
      localStorage.setItem(
        "sv_tmp_create_ticket",
        JSON.stringify(createTicket)
      );
    }
  };

  handleCreateTicketSubmit = id => {
    const context = this.context;
    const receptor = {
      headers: {
        "Content-Type": "application/json",
        publickey: context.sharedDataContext.currentUser.publickey,
        apisid: context.sharedDataContext.currentUser.apisid,
        sessionid: context.sharedDataContext.currentUser.session_id
      }
    };
    const dataCreateTicket = JSON.parse(
      localStorage.getItem("sv_tmp_create_ticket")
    );

    CreateTicketHttpService.createTicket(dataCreateTicket, receptor)
      .then(response => {
        //   console.log("CreateTicketHttpService : ", response);

        if (response && response.data && response.data.status === 202) {
          this.setState({ dataInputTicket: [], fileToUpload: [] });
          this.setState({ objetTicket: "" });
          this.setState({ messageTicket: EditorState.createEmpty() });

          localStorage.removeItem("sv_tmp_create_ticket");
          this.onSocketCreateTicket(response);
        } else {
          // eslint-disable-next-line react/destructuring-assignment
          this.props.handleMessageTicket("error", "--", "--");
        }
      })
      .catch(error => {
        //  console.log("**** print error ****", error);

        // eslint-disable-next-line react/destructuring-assignment
        this.props.handleMessageTicket("error", "--", "--");
      });
  };

  onSocketCreateTicket = response => {
    if (response && response.status === 200) {
      // console.log("onSocketCreateTicket : ", response.data);
      this.props.handleMessageTicket(
        "success",
        response.data.number,
        response.data.id
      );
    }
  };

  initSocketCreateTicket = () => {
    // TODO reactivate later
    /*const { socket } = this.props;
    socket.on(SIO_CREATE_TICKET, response => {
      //  console.log("initSocketCreateTicket : ", response);
      this.onSocketCreateTicket(response);
    });*/
    this.handleCreateTicketSubmit();
  };
  /** End - send customerFiled */

  handleInputSearchChange = event => {
    const { value } = event.currentTarget;
    this.setState({ searchAgent: value });

    const contentSearch = value;

    if (contentSearch !== "") {
      this.setState(prevState => ({
        ...prevState,
        initAgents: this.state.storeInitAgents.filter(
          option =>
            option.firstname
              .toLowerCase()
              .includes(contentSearch.toLowerCase()) ||
            option.lastname.toLowerCase().includes(contentSearch.toLowerCase())
        )
      }));
    } else {
      this.setState(prevState => ({
        ...prevState,
        initAgents: this.state.storeInitAgents
      }));
    }
  };

  //get file
  getFiles = files => {
    // console.log("files from create ticket",files)
    //  console.log("files from create ticket",files)
    this.setState({ fileToUpload: files });
  };

  fileprogresse = progresse => {
    //console.log("progree", progresse);
    let fileNotYest = [];
    fileNotYest = progresse.filter(elm => elm.loadStatus !== 4);
    return fileNotYest.length === 0
      ? this.setState({ successfullUploaded: true })
      : this.setState({ successfullUploaded: false });
  };

  render() {
    const {
      i18n,
      t,
      kind,
      handleMessageTicket,
      ticketSettingsInput
    } = this.props;
    const {
      assegneeModalOpen,
      addAgent,
      dataInputTicket,
      lang,
      successfullUploaded
    } = this.state;
    // console.log("successfullUploaded", successfullUploaded);
    //console.log("errorValidator", this.state.errorValidator.join(""));
    const modalStyle = {
      title: {
        paddingBottom: kind === "channel" ? 0 : "1.125rem"
      }
    };
    //  console.log("fileToUpload from render", this.state.fileToUpload);
    // options: ['inline', 'blockType', 'fontSize', 'fontFamily', 'list', 'textAlign', 'colorPicker', 'link', 'embedded', 'emoji', 'image', 'remove', 'history']
    const context = this.context;
    const customStyles = {
      option: (provided, state) => ({
        ...provided,
        color: state.isSelected ? "#222" : "#222",
        text: "center"
      })
    };

    return (
      <>
        <div className="header-indicator">
          <h3 className="header-indic-title1">
            {t("tickets.details_ticket.header.ticket_table")}
          </h3>
          {" > "}
          <p className="header-indic-title2">
            {t("tickets.create_ticket.text_create")}
          </p>
        </div>
        <div className="ticketnalytics-header">
          <h2 className="dashboard-title">
            {t("tickets.create_ticket.text_create")}
          </h2>
        </div>
        <div className="columns analytics-columns createTicket-conaitner">
          <div className="section1 ">
            <div className="firstInput-container">
              <div className="header-create-ticket">
                <div className="createTicket-div">
                  <img
                    src={ProfileIcon}
                    className="profilepicture-assignee"
                    alt="agent picture"
                  />
                  <span className="createTicket-div-text">
                    {context.sharedDataContext.currentUser.firstname}{" "}
                    {context.sharedDataContext.currentUser.lastname}
                  </span>
                </div>
                <div className="createTicket-div">
                  <span className="createTicket-div-text">
                    {context.sharedDataContext.currentUser.email}
                  </span>
                </div>
              </div>
              <h3 className="customer-text">
                {t("tickets.create_ticket.customer_details")}
              </h3>

              <div className="input-ticket-setting">
                <Formik
                  initialValues={{
                    email: "",
                    text: "",
                    number: "",
                    date: ""
                  }}
                >
                  {({ errors, touched }) => (
                    <Form className="display-input">
                      {this.state.ticketSettingsInputActive === true &&
                        this.state.ticketSettingsInput.length !== 0 &&
                        this.state.ticketSettingsInput.map((item, i) =>
                          i < 4 ? (
                            <div className="div-input" key={i}>
                              <Field
                                className="input"
                                name={i}
                                onChange={e =>
                                  this.handleInputChange(e, item, item.type, i)
                                }
                                onBlur={() => this.handleverification(i)}
                                value={
                                  this.state.dataInputTicket[i] &&
                                  this.state.dataInputTicket[i].value
                                }
                                autoComplete="off"
                                type={
                                  item.type === "number" ? "text" : item.type
                                }
                                placeholder={
                                  item.name === "Nom"
                                    ? "Saisir le nom de famille"
                                    : item.name === "Prénom"
                                    ? "Saisir le prénom"
                                    : item.name === "Adresse email"
                                    ? "Saisir l’adresse email"
                                    : item.name === "Telephone" && lang === "fr"
                                    ? "Saisir le numéro de téléphone"
                                    : `${t("enter_placeholder_input")} ${
                                        item.name
                                      }`
                                }
                              />

                              <span className="alert-danger">
                                {this.state.errorValidator[i] &&
                                  this.state.errorValidator[i].text}
                              </span>
                            </div>
                          ) : (
                            false
                          )
                        )}
                    </Form>
                  )}
                </Formik>
              </div>
            </div>

            <div className="secontInput-container">
              <div>
                <h3 className="textInputcontainer">
                  {t("tickets.create_ticket.ticket_subject")}
                </h3>
                <input
                  className="input //createTicket-large"
                  type="text"
                  placeholder={t("tickets.create_ticket.ticket_subject_input")}
                  onChange={e => this.handleObjetChange(e)}
                  value={this.state.objetTicket}
                  autoComplete="off"
                />
              </div>

              {this.state.prioritySettingActive === true && (
                <div>
                  <h3 className="textInputcontainer">
                    {t("tickets.create_ticket.ticket_priority")}
                  </h3>
                  <div>
                    <Select
                      options={this.state.prioritySetting}
                      onChange={e => this.handlePriorityChange(e)}
                      isSearchable={false}
                      className="ticket-Select"
                      placeholder={t(
                        "tickets.create_ticket.ticket_priority_input"
                      )}
                      styles={customStyles}
                      theme={theme => ({
                        ...theme,
                        colors: {
                          ...theme.colors,
                          primary: "#eee",
                          primary25: "#eee"
                        }
                      })}
                    />
                  </div>
                </div>
              )}

              <h3 className="textInputcontainer">
                {t("tickets.create_ticket.ticket_message")}
              </h3>
              <Editor
                // onChange={(e) => this.handleMessageChange(e)}
                messageTicket={this.state.messageTicket}
                onEditorStateChange={this.onEditorStateChange}
                toolbar={{
                  fontSize: { className: "fontSizetoolbar" },
                  fontFamily: { className: "fontFamilytoolbar" },
                  textAlign: { inDropdown: true },
                  link: { className: "linktoolbar" },
                  emoji: { className: "emojitoolbar" },
                  image: { className: "imagetoolbar" },
                  remove: { className: "removetoolbar" },
                  blockType: { className: "blockTypetoolbar" },
                  embedded: { className: "embeddedtoolbar" },
                  inline: {
                    strikethrough: { className: "strikethroughtoolbar" },
                    monospace: { className: "monospacetoolbar" }
                  },
                  list: {
                    indent: { className: "indenttoolbar" },
                    outdent: { className: "outdenttoolbar" }
                  }
                }}
              />

              <button
                className="Submit-ticketbtn"
                onClick={() => this.handleSubmitCreateTicket()} //
              >
                {t("tickets.create_ticket.ticket_btn_submit")}
              </button>
            </div>
          </div>
          <div className="section2">
            <Upload
              t={t}
              getfileListe={files => this.getFiles(files)}
              fileprogresse={progress => this.fileprogresse(progress)}
              handleMessageTicket={(status, data, idNewTicket) =>
                this.props.handleMessageTicket(status, data, idNewTicket)
              }
            />

            {/* modal assign agent */}
            <div className="assegnee-Container">
              <div className="assign-text-Contain">
                <p style={{ color: "#657288", marginRight: "20px" }}>
                  {t("tickets.create_ticket.assignee")}
                </p>
                <div className="assign-agent-Container">
                  <span
                    className=" assign-agent-btn"
                    onClick={() =>
                      this.setState({
                        assegneeModalOpen: !this.state.assegneeModalOpen
                      })
                    }
                  >
                    +
                  </span>

                  {/* Modal add agent */}
                  <div
                    className="assign-text-modal"
                    style={{
                      display: `${assegneeModalOpen ? "flex" : "none"}`
                    }}
                    ref={node => (this.node = node)}
                  >
                    <h2 className="title assign-modal-title">
                      {t("tickets.create_ticket.assign_agent_ticket")}
                    </h2>
                    <ul className="menu-list menu-list-ticket">
                      {" "}
                      <li className="assign-self">
                        <img src={ProfileIcon} alt="portrait" />
                        <span className="user-name">
                          {t("tickets.create_ticket.assign_ticket_myself")}
                        </span>
                        {this.state.userData.checked ||
                        this.state.userData.checked === true ? (
                          <span
                            className="remove-user"
                            onClick={e =>
                              this.handleRemoveMe(this.state.userData)
                            }
                          >
                            -
                          </span>
                        ) : (
                          <span
                            className="add-user"
                            onClick={e => this.handleAddMe(this.state.userData)}
                          >
                            +
                          </span>
                        )}
                      </li>
                    </ul>
                    <div className="search-box assign-search">
                      <input
                        className="input"
                        type="text"
                        placeholder={t(
                          "tickets.create_ticket.search_agent_input"
                        )}
                        value={this.state.searchAgent}
                        onChange={e => this.handleInputSearchChange(e)}
                      />
                      <img src={SearchIcon} alt="search" />
                    </div>
                    {this.state.storeInitAgents.length === 0 ? (
                      <div>
                        {t("tickets.create_ticket.search.user_list_empty")}
                      </div>
                    ) : (
                      this.state.initAgents.length === 0 && (
                        <div>
                          {t("tickets.create_ticket.search.user_not_found")}
                        </div>
                      )
                    )}
                    <section
                      className="modal-card-body"
                      style={{ width: "100%" }}
                    >
                      {/*  Composant list add Agent Create Ticket */}
                      <ul className=" menu-list menu-list-ticket">
                        {this.state.initAgents &&
                          this.state.initAgents.map((item, i) => (
                            <li key={i}>
                              <img src={ProfileIcon} alt="portrait" />
                              <span className="user-name">
                                {item.firstname} {item.lastname}
                              </span>
                              {item.checked || item.checked === true ? (
                                <span
                                  className="remove-user"
                                  onClick={e =>
                                    this.handleRemoveAgent(item.user_id)
                                  }
                                >
                                  -
                                </span>
                              ) : (
                                <span
                                  className="add-user"
                                  onClick={e =>
                                    this.handleAddAgent(item.user_id)
                                  }
                                >
                                  +
                                </span>
                              )}
                            </li>
                          ))}
                      </ul>
                      {/*  */}
                    </section>
                    <footer className="assign-modal-footer">
                      <button
                        className="button is-primary"
                        aria-label="close"
                        style={{ width: "100%" }}
                        onClick={() =>
                          this.setState({
                            assegneeModalOpen: !this.state.assegneeModalOpen
                          })
                        }
                      >
                        {t("tickets.create_ticket.Assign")}
                      </button>
                    </footer>
                  </div>
                  {/* End Modal add agnet */}
                </div>
              </div>
            </div>
            {/* ** ** */}

            {/* Affich add agent */}
            <div style={{ width: "100%" }}>
              {/* <h2
                className="title assign-modal-title align-point-text"
                style={{ paddingTop: "2rem" }}
              >
                <div className="black-point-head">&nbsp;</div>
                {t("tickets.create_ticket.assign_agent_added")}
              </h2> */}
              <section className="//modal-card-body" style={{ width: "100%" }}>
                <ul className="menu-agent-added menu-list-ticket">
                  {this.state.addAgent &&
                    this.state.addAgent.map((item, i) => (
                      <li key={i}>
                        <img src={ProfileIcon} alt="portrait" />
                        <span
                          className="user-name"
                          style={{ fontWeight: "bold" }}
                        >
                          {item.firstname} {item.lastname}
                        </span>
                      </li>
                    ))}
                </ul>
              </section>

              {/* //<hr style={{ color: "#eee" }} /> */}
            </div>
            {/* End Affich add agnet */}
          </div>
        </div>
        <Footer t={t} />
      </>
    );
  }
}

CreateTicket.propTypes = {
  i18n: PropTypes.shape({}).isRequired,
  t: PropTypes.func.isRequired,
  kind: PropTypes.string.isRequired,
  handleMessageTicket: PropTypes.func.isRequired
};

const CreateTicketWithSocket = props => (
  <DataSocketContext.Consumer>
    {socket => <CreateTicket {...props} socket={socket} />}
  </DataSocketContext.Consumer>
);

export default CreateTicketWithSocket;
