import React, { Component, createRef } from "react";
import PropTypes from "prop-types";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
//import io from "socket.io-client";
import moment from "moment";
import CreateTicket from "../ticketAnalytics/CreateTicket";
import ResolveTicket from "../ticketAnalytics/ResolveTicket";
import PendingTicket from "../ticketAnalytics/PendingTicket";
import TotalTicket from "../ticketAnalytics/TotalTicket";
import MoreIcon from "../../../assets/images/dashboard/more.svg";
import CalendarIcon from "../../../assets/images/dashboard/calendar.svg";
import TicketPagination from "../ticketAnalytics/TicketPagination";
import FilterPagination from "../ticketAnalytics/FilterPagination";
import SearchIcon from "../../../assets/images/profile/search.svg";
import TicketsList from "../ticketAnalytics/TicketsList"; // SortBtn
import SortBtn from "../../../assets/images/tickets/sortBtn.svg";
import {
  GetAllTicketsHttpService,
  UpdateTicketHttpService
} from "../../../services/HttpService";
import { SharedDataContext, DataSocketContext } from "../../app/UseContext";
import Footer from "../../layouts/Footer";

import {
  SOCKET,
  SIO_GET_TICKET,
  SIO_TICKET_STATS,
  SIO_TICKET_INFO,
  SIO_TICKET_DELETE
} from "../../../constants/Constants";

//const socket = io(SOCKET.BASE_URL);

const makeUperCase = str => {
  return str.charAt(0).toLocaleUpperCase() + str.substring(1, str.length);
};

class TicketDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      status: 1,
      allPriority: false,
      high: false,
      medium: false,
      low: false,
      allStatus: false,
      resolved: false,
      pending: false,
      new: false,
      allCategory: false,
      technical: false,
      support: false,
      enquires: false,
      allTickets: [],
      ticketfiltred: [],
      keyfilters: [],
      ticketToList: [],
      ticketToListFiltred: [],
      ticketInfos: {},
      searchticket: "",
      tickets: [],
      searchTicketsFiltres: [],
      ticketStats: { complete: 0, pending: 0, total: 0, new: 0 },
      date: {},
      paramsPagination: {
        sio_channel: SIO_TICKET_INFO,
        page: 1,
        nbre_per_page: 7,
        filter: {}
      },
      pagination: {
        from: 0,
        count: 7
      },
      mount: false
    };
  }
  static contextType = SharedDataContext;

  load = idTicket => {
    const context = this.context;
    const receptor = {
      headers: {
        "Content-Type": "application/json",
        publickey: context.sharedDataContext.currentUser.publickey,
        apisid: context.sharedDataContext.currentUser.apisid,
        sessionid: context.sharedDataContext.currentUser.session_id
      }
    };
    this.initSocketgetInfoTicket(this.state.paramsPagination, receptor);
    this.initSocketgetAllTicket(receptor);
    this.setState({
      searchTicketsFiltres: this.state.searchTicketsFiltres.filter(
        elm => elm.id !== idTicket
      )
    });
  };

  loadAll() {
    //  console.log("hallelujah");
    const context = this.context;
    const receptor = {
      headers: {
        "Content-Type": "application/json",
        publickey: context.sharedDataContext.currentUser.publickey,
        apisid: context.sharedDataContext.currentUser.apisid,
        sessionid: context.sharedDataContext.currentUser.session_id
      }
    };

    // console.log("component didmoint ticket dash");
    this.initSocketgetAllTicket(receptor);
    this.initSocketgetInfoTicket(this.state.paramsPagination, receptor);
    // get new ticket for this day
    let day = new Date();
    let momenday = moment(day);
    let dateDefault = momenday.format("YYYY-MM-DD HH:mm:ss").slice(0, 10);
    //console.log('dateDefault',dateDefault)
    document.addEventListener("mousedown", this.handelClick, false);
    // console.log("dateDefault", dateDefault);
    const date = {
      date_complete_start: dateDefault,
      date_complete_end: dateDefault,
      date_pending_start: dateDefault,
      date_pending_end: dateDefault,
      date_new_start: dateDefault,
      date_new_end: dateDefault,
      date_total_start: dateDefault,
      date_total_end: dateDefault
    };
    //console.log(date);
    this.setState(
      {
        date: date
      },
      () => {
        //console.log('date',this.state.date)
        this.initSocketTicketStats(this.state.date, receptor);
      }
    );
  }
  componentDidMount() {
    this.loadAll();
  }

  componentWillUnmount() {
    document.addEventListener("mousedown", this.handelClick, false);
  }

  //close modam anywhere you click
  handelClick = e => {
    if (this.node && this.node.contains(e.target)) {
      return;
    }
    this.setState({
      isOpen: false
    });
  };

  //get ticket info
  initSocketgetInfoTicket = (data, receptor) => {
    // console.log("initSocketgetInfoTicket ** : ");

    // TODO reactivate later
    /*const { socket } = this.props;
    socket.on(SIO_TICKET_INFO, response => {
      // this.onSocketgetInfoTicket(response.data);
      this.setState({
        ticketToList: response.data.datas,
        ticketInfos: response.data,
        ticketToListFiltred: response.data.datas
      });
    });*/

    GetAllTicketsHttpService.ticketInfo(data, receptor).then(response => {
      //  console.log("ticketStatsPerDate : ", response.data);

      if (response.status === 200 || response.status === 202) {
        this.setState({
          ticketToList: response.data.datas,
          ticketInfos: response.data,
          ticketToListFiltred: response.data.datas
        });
      }
    });
  };
  onSocketgetInfoTicket = response => {
    // console.log("onSocketgetInfoTicket : ", response.data);

    if (response && (response.status === 200 || response.status === 202)) {
      //this.setState({ ticketStats: response.data });
    }
  };

  toNextPage = () => {
    const context = this.context;
    const receptor = {
      headers: {
        "Content-Type": "application/json",
        publickey: context.sharedDataContext.currentUser.publickey,
        apisid: context.sharedDataContext.currentUser.apisid,
        sessionid: context.sharedDataContext.currentUser.session_id
      }
    };
    this.setState(
      {
        paramsPagination: {
          ...this.state.paramsPagination,
          page: this.state.paramsPagination.page + 1
        }
      },
      () => {
        this.initSocketgetInfoTicket(this.state.paramsPagination, receptor);
      }
    );
  };

  toPreviousPage = () => {
    const context = this.context;
    const receptor = {
      headers: {
        "Content-Type": "application/json",
        publickey: context.sharedDataContext.currentUser.publickey,
        apisid: context.sharedDataContext.currentUser.apisid,
        sessionid: context.sharedDataContext.currentUser.session_id
      }
    };
    this.setState(
      {
        paramsPagination: {
          ...this.state.paramsPagination,
          page: this.state.paramsPagination.page - 1
        }
      },
      () => {
        this.initSocketgetInfoTicket(this.state.paramsPagination, receptor);
      }
    );
  };

  gotToPage = index => {
    const context = this.context;
    const receptor = {
      headers: {
        "Content-Type": "application/json",
        publickey: context.sharedDataContext.currentUser.publickey,
        apisid: context.sharedDataContext.currentUser.apisid,
        sessionid: context.sharedDataContext.currentUser.session_id
      }
    };
    // console.log(index)
    this.setState(
      {
        paramsPagination: { ...this.state.paramsPagination, page: index }
      },
      () => {
        this.initSocketgetInfoTicket(this.state.paramsPagination, receptor);
      }
    );
  };

  //TICKET STAT
  initSocketTicketStats = (date, receptor) => {
    // console.log("initSocketTicketStats ** : ");
    const { socket } = this.props;
    socket.on(SIO_TICKET_STATS, response => {
      let ticketStats = {};
      let resp = [];
      resp = Object.entries(response.data);
      resp.length === 0
        ? (ticketStats = {
            agents: 0,
            complete: 0,
            pending: 0,
            new: 0,
            total: 0
          })
        : (ticketStats = response.data);
      //console.log("SIO_TICKET_STATS : ", response);
      this.onSocketTicketStats(response);
      // ticketStats= {agents: 0, complete: 0, pending:0, new: 0, total: 0} : ticketStats=response.data

      this.setState({ ticketStats: ticketStats });
    });

    GetAllTicketsHttpService.ticketStatsPerDate(date, receptor).then(
      response => {
        // console.log("ticketStatsPerDate : ", response);

        if (response.status === 200 || response.status === 202) {
          // console.log("test success : ", response);
        }
      }
    );
  };
  onSocketTicketStats = response => {
    // console.log("onSocketTicketStats : ", response.data);

    if (response && (response.status === 200 || response.status === 202)) {
      this.setState({ ticketStats: response.data });
    }
  };

  //initSocketTicketSettings
  initSocketgetAllTicket = receptor => {
    const { socket } = this.props;
    socket.on(SIO_GET_TICKET, response => {
      //  console.log("SIO_GET_ALL_TICKET : ", response);
      // this.onSocketGetAllTickets(response);
      this.setState({ tickets: response.data });
    });

    GetAllTicketsHttpService.getAllTicket(receptor).then(response => {
      //console.log("getAllTicket : ", response);

      if (response.status === 200 || response.status === 202) {
        // console.log('test success : ', response);
      }
    });
  };

  //onSocketGetAllTickets
  // onSocketGetAllTickets = response => {
  //   // console.log("onSocketGetAllTickets",response.data);
  //   socket.on(SIO_GET_TICKET, response => {
  //     // console.log("SIO_GET_TICKETS : ", response);
  //   });
  //   this.setState({ allTickets: response.data, ticketfiltred: response.data });
  // };

  handleChange = event => {
    //  console.log("event", event.target.value + ":" + event.target.checked);
    let prio = event.target.value;
    let bool = event.target.checked;
    //console.log(prio, "/ ", bool);
    this.setState({ [event.target.value]: event.target.checked }, () => {
      if (bool) {
        if (prio === "allPriority") {
          this.setState(
            {
              high: true,
              medium: true,
              low: true
            },
            () =>
              this.setState({
                paramsPagination: {
                  ...this.state.paramsPagination,
                  filter: {
                    ...this.state.paramsPagination.filter,
                    priority: [
                      "High",
                      "Haute",
                      "Moyenne",
                      "Medium",
                      "Faible",
                      "Low"
                    ]
                  }
                }
              })
          );
        } else if (prio === "allStatus") {
          this.setState({ resolved: true, pending: true, new: true }, () =>
            this.setState({
              paramsPagination: {
                ...this.state.paramsPagination,
                filter: {
                  ...this.state.paramsPagination.filter,
                  status: [
                    "Resolved",
                    "Résolu",
                    "Pending",
                    "En attente",
                    "Nouveau",
                    "New"
                  ]
                }
              }
            })
          );
        } else if (prio === "allCategory") {
          this.setState(
            {
              technical: true,
              support: true,
              enquires: true
            },
            () =>
              this.setState({
                paramsPagination: {
                  ...this.state.paramsPagination,
                  filter: {
                    ...this.state.paramsPagination.filter,
                    category: [
                      "Technical",
                      "Technique",
                      "Support",
                      "Enquires",
                      "Demande"
                    ]
                  }
                }
              })
          );
        } else {
          let keyfilters = this.state.keyfilters;

          if (prio === "high") keyfilters.push("High", "Haute");
          if (prio === "medium") keyfilters.push("Medium", "Moyenne");
          if (prio === "low") keyfilters.push("Low", "Faible");
          if (prio === "resolved") keyfilters.push("Resolved", "Résolu");
          if (prio === "pending") keyfilters.push("Pending", "En attente");
          if (prio === "new") keyfilters.push("New", "Nouveau");
          if (prio === "technical") keyfilters.push("Technical", "Technique");
          if (prio === "support") keyfilters.push("Support");
          if (prio === "enquires") keyfilters.push("Enquires", "Demande");
          this.setState(
            {
              keyfilters: keyfilters // this.state.keyfilters.concat(prio)
            },
            () => {
              let filter = { priority: [], status: [], category: [] };
              for (let i of this.state.keyfilters) {
                if (
                  i === "High" ||
                  i === "Haute" ||
                  i === "Moyenne" ||
                  i === "Medium" ||
                  i === "Low" ||
                  i === "Faible"
                )
                  filter.priority.push(makeUperCase(i));
                else if (
                  i === "Resolved" ||
                  i === "Résolu" ||
                  i === "Pending" ||
                  i === "En attente" ||
                  i === "New" ||
                  i === "Nouveau"
                )
                  filter.status.push(makeUperCase(i));
                else if (
                  i === "Technical" ||
                  i === "Technique" ||
                  i === "Support" ||
                  i === "Enquires" ||
                  i === "Demande"
                )
                  filter.category.push(makeUperCase(i));
              }
              this.setState({
                paramsPagination: { ...this.state.paramsPagination, filter }
              });
            }
          );
        }
      } else {
        if (prio === "allPriority") {
          this.setState(
            {
              high: false,
              medium: false,
              low: false
            },
            () =>
              this.setState({
                paramsPagination: {
                  ...this.state.paramsPagination,
                  filter: {
                    ...this.state.paramsPagination.filter,
                    priority: []
                  }
                },
                keyfilters: this.state.keyfilters.filter(
                  elm =>
                    elm !== "High" &&
                    elm !== "Haute" &&
                    elm !== "Medium" &&
                    elm !== "Moyenne" &&
                    elm !== "low" &&
                    elm !== "Faible"
                )
              })
          );
        } else if (prio === "allStatus") {
          this.setState({ resolved: false, pending: false, new: false }, () =>
            this.setState({
              paramsPagination: {
                ...this.state.paramsPagination,
                filter: { ...this.state.paramsPagination.filter, status: [] }
              },
              keyfilters: this.state.keyfilters.filter(
                elm =>
                  elm !== "Resolved" &&
                  elm !== "Résolu" &&
                  elm !== "Pending" &&
                  elm !== "En attente" &&
                  elm !== "New" &&
                  elm !== "Nouveau"
              )
            })
          );
        } else if (prio === "allCategory") {
          this.setState(
            {
              technical: false,
              support: false,
              enquires: false
            },
            () =>
              this.setState({
                paramsPagination: {
                  ...this.state.paramsPagination,
                  filter: {
                    ...this.state.paramsPagination.filter,
                    category: []
                  }
                },
                keyfilters: this.state.keyfilters.filter(
                  elm =>
                    elm !== "Technical" &&
                    elm !== "Technique" &&
                    elm !== "support" &&
                    elm !== "Demande" &&
                    elm !== "enquires"
                )
              })
          );
        } else {
          let keyfilters = this.state.keyfilters;
          //console.log(keyfilters)
          if (prio === "high")
            keyfilters = keyfilters.filter(
              elm => elm !== "High" && elm !== "Haute"
            );
          if (prio === "medium")
            keyfilters = keyfilters.filter(
              elm => elm !== "Medium" && elm !== "Moyenne"
            ); //keyfilters.push("Medium","Moyenne")
          if (prio === "low")
            keyfilters = keyfilters.filter(
              elm => elm !== "Low" && elm !== "Faible"
            ); //keyfilters.push("Low","Faible")
          if (prio === "resolved")
            keyfilters = keyfilters.filter(
              elm => elm !== "Resolved" && elm !== "Résolu"
            ); //keyfilters.push("Resolved","Résolu");
          if (prio === "pending")
            keyfilters = keyfilters.filter(
              elm => elm !== "Pending" && elm !== "En attente"
            ); //keyfilters.push("Pending","En attente");
          if (prio === "new")
            keyfilters = keyfilters.filter(
              elm => elm !== "New" && elm !== "Nouveau"
            ); //keyfilters.push("New" ,"Nouveau");
          if (prio === "technical")
            keyfilters = keyfilters.filter(
              elm => elm !== "Technical" && elm !== "Technique"
            ); //keyfilters.push("Technical","Technique")
          if (prio === "support")
            keyfilters = keyfilters.filter(elm => elm !== "Support"); //keyfilters.push("Support")
          if (prio === "enquires")
            keyfilters = keyfilters.filter(
              elm => elm !== "Enquires" && elm !== "Demande"
            ); //keyfilters.push("Enquires","Demande")
          this.setState(
            {
              keyfilters: keyfilters // this.state.keyfilters.filter(elm => elm !== prio)
            },
            () => {
              let filter = { priority: [], status: [], category: [] };
              for (let i of this.state.keyfilters) {
                if (
                  i === "High" ||
                  i === "Haute" ||
                  i === "Moyenne" ||
                  i === "Medium" ||
                  i === "Low" ||
                  i === "Faible"
                )
                  filter.priority.push(makeUperCase(i));
                else if (
                  i === "Resolved" ||
                  i === "Résolu" ||
                  i === "Pending" ||
                  i === "En attente" ||
                  i === "New" ||
                  i === "Nouveau"
                )
                  filter.status.push(makeUperCase(i));
                else if (
                  i === "Technical" ||
                  i === "Technique" ||
                  i === "Support" ||
                  i === "Enquires" ||
                  i === "Demande"
                )
                  filter.category.push(makeUperCase(i));
                // let filter = { priority: [], status: [], category: [] };
                // for (let i of this.state.keyfilters) {
                //   if (i === "high" || i === "medium" || i === "low")
                //     filter.priority.push(makeUperCase(i));
                //   else if (i === "resolved" || i === "pending" || i === "new")
                //     filter.status.push(makeUperCase(i));
                //   else if (
                //     i === "technical" ||
                //     i === "support" ||
                //     i === "enquires"
                //   )
                //     filter.category.push(makeUperCase(i));
              }
              this.setState({
                paramsPagination: { ...this.state.paramsPagination, filter }
              });
            }
          );
        }
      }
    });
  };

  //applyFilter
  applyFilter = () => {
    const context = this.context;
    const receptor = {
      headers: {
        "Content-Type": "application/json",
        publickey: context.sharedDataContext.currentUser.publickey,
        apisid: context.sharedDataContext.currentUser.apisid,
        sessionid: context.sharedDataContext.currentUser.session_id
      }
    };
    this.initSocketgetInfoTicket(this.state.paramsPagination, receptor);
  };

  clearfilter = () => {
    const context = this.context;
    const receptor = {
      headers: {
        "Content-Type": "application/json",
        publickey: context.sharedDataContext.currentUser.publickey,
        apisid: context.sharedDataContext.currentUser.apisid,
        sessionid: context.sharedDataContext.currentUser.session_id
      }
    };
    this.setState({
      allPriority: false,
      high: false,
      medium: false,
      low: false,
      allStatus: false,
      resolved: false,
      pending: false,
      new: false,
      allCategory: false,
      technical: false,
      support: false,
      enquires: false
    });
    this.setState(
      {
        paramsPagination: {
          sio_channel: SIO_TICKET_INFO,
          page: 1,
          nbre_per_page: 7,
          filter: {}
        },
        keyfilters: []
      },
      () => {
        this.initSocketgetInfoTicket(this.state.paramsPagination, receptor);
      }
    );
  };

  closeFilter = () => {
    this.setState({
      isOpen: false,
      keyfilters: []
    });
    this.clearfilter();
  };

  //filter Total ticket
  ticketTotalDateFilter = (timeStart, timeEnd) => {
    const context = this.context;
    const receptor = {
      headers: {
        "Content-Type": "application/json",
        publickey: context.sharedDataContext.currentUser.publickey,
        apisid: context.sharedDataContext.currentUser.apisid,
        sessionid: context.sharedDataContext.currentUser.session_id
      }
    };
    // let day = new Date();
    // let momenday = moment(day);
    // let dateDefault = momenday.format("YYYY-MM-DD HH:mm:ss").slice(0, 10);
    const timeStartmoment = moment(timeStart)
      .format("YYYY-MM-DD HH:mm:ss")
      .slice(0, 10);
    const timeEndmoment = moment(timeEnd)
      .format("YYYY-MM-DD HH:mm:ss")
      .slice(0, 10);
    this.setState(
      {
        date: {
          ...this.state.date,
          date_total_start: timeStartmoment,
          date_total_end: timeEndmoment
        }
      },
      () => {
        this.initSocketTicketStats(this.state.date, receptor);
      }
    );
  };

  //COMPLETE TICKET DATE FILTER
  ticketCompleteDateFilter = (timeStart, timeEnd) => {
    const context = this.context;
    const receptor = {
      headers: {
        "Content-Type": "application/json",
        publickey: context.sharedDataContext.currentUser.publickey,
        apisid: context.sharedDataContext.currentUser.apisid,
        sessionid: context.sharedDataContext.currentUser.session_id
      }
    };
    // let day = new Date();
    // let momenday = moment(day);
    // let dateDefault = momenday.format("YYYY-MM-DD HH:mm:ss").slice(0, 10);

    const timeStartmoment = moment(timeStart)
      .format("YYYY-MM-DD HH:mm:ss")
      .slice(0, 10);
    const timeEndmoment = moment(timeEnd)
      .format("YYYY-MM-DD HH:mm:ss")
      .slice(0, 10);

    this.setState(
      {
        date: {
          ...this.state.date,
          date_complete_start: timeStartmoment,
          date_complete_end: timeEndmoment
        }
      },
      () => {
        this.initSocketTicketStats(this.state.date, receptor);
      }
    );
  };

  //PENDING TICKET DATE FILTER

  ticketPendingDateFilter = (timeStart, timeEnd) => {
    const context = this.context;
    const receptor = {
      headers: {
        "Content-Type": "application/json",
        publickey: context.sharedDataContext.currentUser.publickey,
        apisid: context.sharedDataContext.currentUser.apisid,
        sessionid: context.sharedDataContext.currentUser.session_id
      }
    };
    // let day = new Date();
    // let momenday = moment(day);
    // let dateDefault = momenday.format("YYYY-MM-DD HH:mm:ss").slice(0, 10);

    const timeStartmoment = moment(timeStart)
      .format("YYYY-MM-DD HH:mm:ss")
      .slice(0, 10);
    const timeEndmoment = moment(timeEnd)
      .format("YYYY-MM-DD HH:mm:ss")
      .slice(0, 10);
    this.setState(
      {
        date: {
          ...this.state.date,
          date_pending_start: timeStartmoment,
          date_pending_end: timeEndmoment
        }
      },
      () => {
        this.initSocketTicketStats(this.state.date, receptor);
      }
    );
  };

  handleSearch = e => {
    this.setState({ searchticket: e.target.value }, () => {
      this.setState({
        searchTicketsFiltres: this.state.tickets.filter(
          elm =>
            elm.number.includes(this.state.searchticket) ||
            elm.subject
              .toLowerCase()
              .includes(this.state.searchticket.toLowerCase())
        )
      });
    });
  };

  //DELETE TICKET
  deleteTicket = (IdTicket, id) => {
    const context = this.context;
    const receptor = {
      headers: {
        "Content-Type": "application/json",
        publickey: context.sharedDataContext.currentUser.publickey,
        apisid: context.sharedDataContext.currentUser.apisid,
        sessionid: context.sharedDataContext.currentUser.session_id
      }
    };
    //console.log(IdTicket,'  ',id)
    this.initSocketDeleteTicket(IdTicket, id, receptor);
    //this.handleAddRessourceModal()
  };

  //SOCKET FOR DELETE TICKET
  //get ticket info
  initSocketDeleteTicket = (idTicket, id, receptor) => {
    const { socket } = this.props;
    // console.log("initSocketDeleteTicket ** : ");

    socket.on(SIO_TICKET_DELETE, response => {
      //   console.log("SIO_TICKET_INFO : ", response);
      // this.onSocketDeleteTicket(response.data);
    });

    UpdateTicketHttpService.deleteTicket(idTicket, receptor).then(response => {
      // console.log("ticketStatsPerDate : ", response.data);

      if (response.status === 200 || response.status === 202) {
        // console.log("test-Delete-success : ", response);
        // Reload After ticket Delete

        this.load(idTicket);
        this.props.handleMessageTicket("test-Delete-success", id, idTicket);
      } else {
        this.props.handleMessageTicket("Error-Delete-ticket", id, idTicket);
      }
    });
  };
  // onSocketDeleteTicket = response => {
  //   //   console.log("onSocketDeleteTicket : ", response);

  //   if (response && (response.status === 200 || response.status === 202)) {
  //     // console.log(response.data);
  //   }
  // };

  //filter Pagination
  gotToPageOnsearch = index => {
    this.setState({
      pagination: { ...this.state.pagination, from: index * 7 }
    });
  };

  render() {
    const {
      i18n,
      t,
      kind,
      handleCreateTicket,
      handleMessageTicket
    } = this.props;

    const {
      isOpen,
      allTickets,
      ticketStats,
      date,
      ticketfiltred,
      keyfilters,
      ticketToList,
      ticketToListFiltred,
      ticketInfos,
      searchTicketsFiltres,
      searchticket,
      pagination: { from, count }
    } = this.state;
    // console.log('searchTicketsFiltres',searchTicketsFiltres.length)
    return (
      <>
        <div className="ticketnalytics-header">
          <h2 className="dashboard-title titledashboard">
            {kind === "tickets"
              ? t("tickets.tickets_overview")
              : kind === "dashboard"
              ? t("dashboard.dashboard_overview")
              : t("settings.settings_overview")}
          </h2>
          <button
            className="create_ticketbtn"
            onClick={() => handleCreateTicket()}
          >
            + {t("tickets.ticket_btn_create").toUpperCase()}
          </button>
        </div>
        <div className="columns analytics-columns">
          <CreateTicket
            t={t}
            CalendarIcon={CalendarIcon}
            MoreIcon={MoreIcon}
            ticketStats={ticketStats.new}
          />

          <ResolveTicket
            t={t}
            CalendarIcon={CalendarIcon}
            MoreIcon={MoreIcon}
            ticketStats={ticketStats.complete}
            ticketCompleteDateFilter={(start, end) =>
              this.ticketCompleteDateFilter(start, end)
            }
          />

          <PendingTicket
            t={t}
            CalendarIcon={CalendarIcon}
            MoreIcon={MoreIcon}
            ticketStats={ticketStats.pending}
            ticketPendingDateFilter={(start, end) =>
              this.ticketPendingDateFilter(start, end)
            }
          />

          <TotalTicket
            t={t}
            CalendarIcon={CalendarIcon}
            MoreIcon={MoreIcon}
            ticketStats={ticketStats.total}
            ticketTotalDateFilter={(start, end) =>
              this.ticketTotalDateFilter(start, end)
            }
          />
        </div>
        <div className="columns chart-columns ">
          <div className="column column-chart ticketContainer container-dashboard-ticket ">
            <div className="searchBar">
              <button className="buttonserch">
                <img className="view-more" src={SearchIcon} alt="SearchIcon" />
              </button>

              <input
                className="input input-search"
                type="text"
                placeholder={t("tickets.search_placeholder")}
                onChange={this.handleSearch}
                style={{ padding: "3px" }}
              />

              <button
                className="button buttonFilter"
                onClick={() => {
                  this.setState({
                    isOpen: !isOpen
                  });
                  //this.clearfilter();
                }}
              >
                <img className="view-more" src={SortBtn} alt="Sort Button" />
                {t("tickets.search_filter")}
                <img className="view-more" src={MoreIcon} alt="caneldar" />
              </button>
            </div>
            <div
              className="modalSerach"
              style={{ display: `${isOpen ? "flex" : "none"}` }}
              ref={node => (this.node = node)}
            >
              <div className="priorityContainer">
                {" "}
                <span className="text-filter">
                  {" "}
                  {t("tickets.search_filter_content.priority")}{" "}
                </span>
                <FormGroup row>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.allPriority}
                        onChange={this.handleChange}
                        value="allPriority"
                        color="primary"
                      />
                    }
                    label={t(
                      "tickets.search_filter_content.priority_filter.all"
                    )}
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.high}
                        onChange={this.handleChange}
                        value="high"
                        color="primary"
                      />
                    }
                    label={t(
                      "tickets.search_filter_content.priority_filter.high"
                    )}
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.medium}
                        onChange={this.handleChange}
                        value="medium"
                        color="primary"
                      />
                    }
                    label={t(
                      "tickets.search_filter_content.priority_filter.medium"
                    )}
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.low}
                        onChange={this.handleChange}
                        value="low"
                        color="primary"
                      />
                    }
                    label={t(
                      "tickets.search_filter_content.priority_filter.low"
                    )}
                  />
                </FormGroup>
              </div>
              <div className="statusContainer">
                {" "}
                <span className="text-filter">
                  {" "}
                  {t("tickets.search_filter_content.status")}{" "}
                </span>
                <FormGroup row>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.allStatus}
                        onChange={this.handleChange}
                        value="allStatus"
                        color="primary"
                      />
                    }
                    label={t("tickets.search_filter_content.status_filter.all")}
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.new}
                        onChange={this.handleChange}
                        value="new"
                        color="primary"
                      />
                    }
                    label={t("tickets.search_filter_content.status_filter.new")}
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.pending}
                        onChange={this.handleChange}
                        value="pending"
                        color="primary"
                      />
                    }
                    label={t(
                      "tickets.search_filter_content.status_filter.pending"
                    )}
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.resolved}
                        onChange={this.handleChange}
                        value="resolved"
                        color="primary"
                      />
                    }
                    label={t(
                      "tickets.search_filter_content.status_filter.resolve"
                    )}
                  />
                </FormGroup>
              </div>
              <div className="CategorieContainer">
                {" "}
                <span className="text-filter">
                  {" "}
                  {t("tickets.search_filter_content.category")}{" "}
                </span>
                <FormGroup row>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.allCategory}
                        onChange={this.handleChange}
                        value="allCategory"
                        color="primary"
                      />
                    }
                    label={t(
                      "tickets.search_filter_content.category_filter.all"
                    )}
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.technical}
                        onChange={this.handleChange}
                        value="technical"
                        color="primary"
                      />
                    }
                    label={t(
                      "tickets.search_filter_content.category_filter.technical"
                    )}
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.support}
                        onChange={this.handleChange}
                        value="support"
                        color="primary"
                      />
                    }
                    label={t(
                      "tickets.search_filter_content.category_filter.support"
                    )}
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.enquires}
                        onChange={this.handleChange}
                        value="enquires"
                        color="primary"
                      />
                    }
                    label={t(
                      "tickets.search_filter_content.category_filter.enquires"
                    )}
                  />
                </FormGroup>
              </div>
              <div className="modalSerachSetting">
                <p className="clear-Filter" onClick={this.clearfilter}>
                  {t("tickets.search_filter_content.clear_all_filters")}
                </p>
                <p>
                  <span
                    style={{
                      color: "#94A4BE",
                      marginRight: "32px",
                      cursor: "pointer"
                    }}
                    onClick={() => this.setState({ isOpen: false })}
                  >
                    {t("tickets.search_filter_content.cancel")}
                  </span>
                  <span
                    style={{ color: "##0089E1", cursor: "pointer" }}
                    onClick={() => this.applyFilter()}
                  >
                    {t("tickets.search_filter_content.apply")}
                  </span>
                </p>
              </div>
            </div>
            <TicketsList
              t={t}
              handleMessageTicket={handleMessageTicket}
              allTickets={
                searchticket === ""
                  ? ticketToListFiltred
                  : searchTicketsFiltres.slice(from, from + count)
              }
              deleteTicket={(isTicket, id) => this.deleteTicket(isTicket, id)}
              page={this.state.ticketInfos.page}
              //  searchTicketsFiltres={searchTicketsFiltres}
              // searchticket={searchticket}
            />
          </div>
        </div>
        {searchticket === "" ? (
          <TicketPagination
            t={t}
            ticketInfot={ticketInfos}
            gotToPage={index => this.gotToPage(index)}
            nextPage={() => this.toNextPage()}
            previousPage={() => this.toPreviousPage()}
          />
        ) : (
          <FilterPagination
            t={t}
            from={from}
            count={count}
            total={
              searchTicketsFiltres.length % 7 === 0
                ? searchTicketsFiltres.length / 7
                : Math.floor(searchTicketsFiltres.length / 7) + 1
            }
            totalPage={searchTicketsFiltres.length}
            // onChangeCount={count => {
            //   this.setState({
            //     pagination: {
            //       ...this.state.pagination,
            //       count
            //     }
            //   })
            // }}
            gotToPageOnsearch={index => this.gotToPageOnsearch(index)}
            onPrev={() =>
              this.setState({
                pagination: {
                  ...this.state.pagination,
                  from: Math.max(0, from - count)
                }
              })
            }
            onNext={() => {
              if (from + count >= searchTicketsFiltres.length) return;
              this.setState({
                pagination: {
                  ...this.state.pagination,
                  from: from + count
                }
              });
            }}
          />
        )}
        <Footer t={t} />
      </>
    );
  }
}

TicketDashboard.propTypes = {
  i18n: PropTypes.shape({}).isRequired,
  t: PropTypes.func.isRequired,
  kind: PropTypes.string.isRequired,
  handleCreateTicket: PropTypes.func.isRequired,
  handleMessageTicket: PropTypes.func.isRequired
};

const TicketDashboardWithSocket = props => (
  <DataSocketContext.Consumer>
    {socket => <TicketDashboard {...props} socket={socket} />}
  </DataSocketContext.Consumer>
);

export default TicketDashboardWithSocket;
