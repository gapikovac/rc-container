import React, { useEffect, useState, useContext } from "react";
import PropTypes from "prop-types";

// Use Socket io - import
//import io from "socket.io-client";

import "react-tabs/style/react-tabs.css";
import { Tab, Tabs as Tabber, TabList, TabPanel } from "react-tabs";
import Content from "./Content";

/* START $$$$$$$$$$$$$$$$$$$$$$$$$$$$$ */
import { TicketSettingsHttpService } from "../../../services/HttpService";
import { SOCKET, SIO_TICKET_SETTINGS } from "../../../constants/Constants";
/* END $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ */

import { SharedDataContext, DataSocketContext } from "../../app/UseContext";
import { CONSTANT } from "../../../constants/browser";
import Footer from "../../layouts/Footer";
//const socket = io(SOCKET.BASE_URL);

const Tabs = props => {
  const { t, kind, i18n, socket } = props;

  const cardStyle = {
    titleContainer: {
      padding: "0 6rem"
    },
    h2: {
      padding: "1.5rem 0",
      fontSize: "2rem"
    },
    card: {
      width: "initial",
      maxWidth: "initial"
    }
  };

  const { sharedDataContext } = useContext(SharedDataContext); // setSharedDataContext

  const [ticketSettings, setTicketSettings] = useState([]);

  const { priority, status, category, customer_information } = ticketSettings;

  /* START $$$$$$$$$$$$$$$$$$$$$$$$$$$$$ */
  const onSocketGetTicketSettings = response => {
    //  console.log("onSocketGetTicketSettings : ", response);

    if (response && (response.status === 200 || response.status === 202)) {
      if (localStorage.getItem(CONSTANT.LOCAL_STORAGE_LANG_KEY) === "en") {
        setTicketSettings(response.data[0].lang_en);
      }
      if (localStorage.getItem(CONSTANT.LOCAL_STORAGE_LANG_KEY) === "fr") {
        setTicketSettings(response.data[0].lang_fr);
      }
    }
  };

  const initSocketTicketSettings = () => {
    //  console.log("initSocketTicketSettings : **** ");
    const receptor = {
      headers: {
        "Content-Type": "application/json",
        publickey:
          sharedDataContext && sharedDataContext.currentUser
            ? sharedDataContext.currentUser.publickey
            : false,
        apisid:
          sharedDataContext && sharedDataContext.currentUser
            ? sharedDataContext.currentUser.apisid
            : false,
        sessionid:
          sharedDataContext && sharedDataContext.currentUser
            ? sharedDataContext.currentUser.session_id
            : false
      }
    };

    // TODO Reactivate later
    /*socket.on(SIO_TICKET_SETTINGS, response => {
      //  console.log("SIO_TICKET_SETTINGS : ", response);
      onSocketGetTicketSettings(response);
    });*/

    TicketSettingsHttpService.getDatasTicketSettings(receptor).then(
      response => {
        //console.log("getDatasTicketSettings : ", response);

        if (response.status === 200 || response.status === 202) {
          onSocketGetTicketSettings(response);
        } else {
          //  console.log("test error : ", response);
        }
      }
    );
  };
  /* END $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ */

  useEffect(() => {
    if (sharedDataContext.socketConnected) {
      initSocketTicketSettings();
    }
    return () => {
      // cleanup
    };
  }, [sharedDataContext]);

  return (
    <>
      <div style={cardStyle.titleContainer}>
        <h2 style={cardStyle.h2}>
          {kind === "dashboard"
            ? t("dashboard.dashboard_overview")
            : t("settings.settings_overview")}
        </h2>
        <div className="card tickets-card">
          <Tabber>
            <TabList>
              <Tab>{t("settings.ticket_settings")}</Tab>
            </TabList>
            <TabPanel>
              <Content
                kind={kind}
                t={t}
                i18n={i18n}
                priority={priority}
                status={status}
                category={category}
                customerInformation={customer_information}
              />
            </TabPanel>
          </Tabber>
        </div>
      </div>
      <Footer t={t} />
    </>
  );
};

Tabs.propTypes = {
  t: PropTypes.func.isRequired,
  kind: PropTypes.string.isRequired
};
const TabsWithSocket = props => (
  <DataSocketContext.Consumer>
    {socket => <Tabs {...props} socket={socket} />}
  </DataSocketContext.Consumer>
);
export default TabsWithSocket;
