import React from "react";
import PropTypes from "prop-types";

const TicketsCategoryContent = props => {
  const {
    t,
    handleCloseRessourceModal,
    handleAddRessourceModal,
    categoryItems
  } = props;

  return (
    <>
      <div className="ticket-content">
        <div className="ticket-priorities">
          <div
            className="ticket-prio-item-Container"
            style={{ height: "50px" }}
          >
            <div className="ticket-prio-item">
              <span className="ticket-prio-item-text">
                {t("settings.tickets_category_content.technical")}
              </span>
            </div>
          </div>
          <div
            className="ticket-prio-item-Container"
            style={{ height: "50px" }}
          >
            <div className="ticket-prio-item">
              <span className="ticket-prio-item-text">
                {t("settings.tickets_category_content.customer_care")}
              </span>
            </div>
          </div>
          <div
            className="ticket-prio-item-Container"
            style={{ height: "50px" }}
          >
            <div className="ticket-prio-item">
              <span className="ticket-prio-item-text">
                {t("settings.tickets_category_content.enquires")}
              </span>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

TicketsCategoryContent.propTypes = {
  t: PropTypes.func.isRequired,
  handleCloseRessourceModal: PropTypes.func.isRequired,
  handleAddRessourceModal: PropTypes.func.isRequired,
  categoryItems: PropTypes.arrayOf.isRequired
};

export default TicketsCategoryContent;
