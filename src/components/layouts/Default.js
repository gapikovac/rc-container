import React from "react";
import PropTypes from "prop-types";
import { Container, Row, Col } from "shards-react";

import MainNavbar from "./MainNavbar/MainNavbar";
import MainSidebar from "./MainSidebar/MainSidebar";
import Header from "./MainNavbar/NavbarNav/Header";
import MainFooter from "./MainFooter";

const customWelcomeClass = {
  padding: "0.8rem 1rem 0.8rem 5rem",
  borderBottom: "1px solid rgb(229, 229, 229)",
  backgroundColor: "rgb(255, 255, 255)"
};

const DefaultLayout = ({ children, noNavbar, noFooter, noMenubar }) => (
  <>
    {noMenubar ? (
      <div>
        <Header kind={"welcome"} customWelcomeClass={customWelcomeClass} />
        <div>{children}</div>
      </div>
    ) : (
      <Container fluid>
        <Row>
          <MainSidebar />
          <Col
            className="main-content p-0"
            lg={{ size: 10, offset: 2 }}
            md={{ size: 9, offset: 3 }}
            sm="12"
            tag="main"
          >
            {!noNavbar && <MainNavbar />}
            <div style={{ margin: "1.5rem 6.2rem 2rem 6.2rem" }}>
              {children}
            </div>
            {!noFooter && <MainFooter />}
          </Col>
        </Row>
      </Container>
    )}
  </>
);

DefaultLayout.propTypes = {
  /**
   * Whether to display the navbar, or not.
   */
  noNavbar: PropTypes.bool,
  /**
   * Whether to display the footer, or not.
   */
  noFooter: PropTypes.bool
};

DefaultLayout.defaultProps = {
  noNavbar: false,
  noFooter: false,
  noMenuBar: false
};

export default DefaultLayout;
