import React, { useContext, useEffect, useState } from "react";
import Select from "react-select";
import { options as optionsLang } from "../../../../configs/options";
import { CONSTANTS_LANG } from "../../../../constants/Constants";
import BrowserLanguage from "../../../../utils/BrowserLanguage";

import DesktopLogo from "../../../../assets/images/logo/Rightcare_logo.svg";
import ProfileIcon from "../../../../assets/images/profile/user.svg";

import MenuHeader from "../../../../assets/images/dashboard/header/menu.svg";

import { SharedDataContext } from "../../../app/UseContext";

const Header = ({ kind, customWelcomeClass }) => {
  const topNavCustomStyle = {
    navBar: {
      minHeight: "6.2rem",
      padding: ".8rem 1rem .8rem 5rem !important",
      borderBottom: kind !== "app" ? "1px solid #e5e5e5" : "0",
      backgroundColor: kind === "dashboard" ? "rgb(255,255,255)" : "#fff"
    },
    isPrimary: {
      marginRight: "5rem",
      marginBottom: 0,
      fontSize: "18px"
    },
    selectLang: {
      borderRadius: "34px",
      boxShadow: "0 3px 20px 0 rgba(137, 137, 137, 0.24)",
      border: "solid 1px #e5e5e5"
    },
    control: {
      width: "9rem"
    },
    profile: {
      width: "3.0625rem",
      height: "3.0625rem",
      border: "solid 1px #e3e3e3",
      borderRadius: "50%",
      maxHeight: "3.0625rem",
      margin: "0 .5rem"
    },
    userName: {
      fontSize: "0.875rem",
      color: "#657288",
      margin: "0 .5rem",
      width: "15rem",
      textAlign: "center"
    },
    search: {
      backgroundColor: kind === "dashboard" ? "#fafbfd" : "#fff"
    },
    select: {
      width: "5rem"
    }
  };

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      textTransform: "uppercase",
      fontSize: "13px",
      backgroundColor: state.isSelected ? "#ddd" : "#fff",
      color: state.isSelected ? "#000" : "#000"
    })
  };

  const { sharedDataContext, setSharedDataContext } = useContext(
    SharedDataContext
  );

  const [defaultLang, setDefaultLang] = useState(optionsLang[0]);

  useEffect(() => {
    if (localStorage.getItem(CONSTANTS_LANG.LOCAL_STORAGE_LANG_KEY)) {
      setDefaultLang(
        localStorage.getItem(CONSTANTS_LANG.LOCAL_STORAGE_LANG_KEY) === "fr"
          ? optionsLang[1]
          : optionsLang[0]
      );
    }
  }, []);

  const changeLang = lang => {
    const { i18n } = sharedDataContext.i18n;
    if (!lang) {
      const tempLang = BrowserLanguage.getDefaultLanguage();
      lang = tempLang === "en" ? "fr" : "en";
    }
    setDefaultLang(lang);

    if (BrowserLanguage.setLanguage(lang.value)) {
      // Reload page if browser support localStorage
      window.location.reload(); // Relaod app after langue change
    } else {
      // Change language at runtime if localStorage not found
      i18n.changeLanguage(lang); // Need for change language at runtime
    }
  };

  return (
    <nav
      className="navbar"
      role="navigation"
      aria-label="main navigation"
      style={kind === "welcome" ? customWelcomeClass : topNavCustomStyle.navBar}
    >
      {kind !== "dashboard" && kind !== "settings" && kind !== "tickets" && (
        <div
          className="navbar-brand"
          style={{ display: "flex", alignItems: "center" }}
        >
          <a className="navbar-item" href="/">
            <img alt="logo icon" src={DesktopLogo} />
          </a>
          <p className="left-menu-logo-text">RightCare</p>
        </div>
      )}

      <div id="navbarBasicExample" className="navbar-menu">
        {/* Zone d'affichage du user connecter & select langue */}
        <div className="navbar-end">
          <div className="navbar-item">
            {sharedDataContext.userLogged ? (
              <span style={topNavCustomStyle.userName}>
                {sharedDataContext.currentUser.firstname +
                  " " +
                  sharedDataContext.currentUser.lastname}{" "}
              </span>
            ) : (
              <img
                alt="profil icon"
                src={ProfileIcon}
                style={topNavCustomStyle.profile}
              />
            )}
            <Select
              styles={customStyles}
              options={optionsLang}
              value={defaultLang}
              onChange={changeLang}
              className="App-Select"
              isSearchable={false}
            />
            <img alt="menu-header" className="menu-header" src={MenuHeader} />
          </div>
        </div>
      </div>
    </nav>
  );
};

export default Header;
