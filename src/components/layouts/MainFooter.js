import React, { useContext } from "react";
import PropTypes from "prop-types";
import { Container, Row, Nav, NavItem, NavLink } from "shards-react";
import { Link } from "react-router-dom";
import { SharedDataContext } from "../app/UseContext";

import logoRC from "../../assets/images/logo/IogoRC.png";
import moment from "moment";

function newDate() {
  let day = new Date();
  let momenday = moment(day);
  return momenday.format("YYYY-MM-DD HH:mm:ss").slice(0, 4);
}

function openUrl(url) {
  return window.open(url);
}

const MainFooter = () => {
  const { sharedDataContext, setSharedDataContext } = useContext(
    SharedDataContext
  );
  const t = sharedDataContext.t;
  return (
    <div className="footer-container">
      <div className="footer-list">
        <p
          className="footer-text-list"
          style={{ color: "#0089E1", cursor: "default" }}
        >
          RightCare (c) {newDate()}
        </p>
        <p
          onClick={() =>
            openUrl("https://right-com.com/solution/customer-care/")
          }
          className="footer-text-list"
        >
          {t("footer.learn_about")}
        </p>
        <p
          onClick={() => openUrl("https://right-com.com/")}
          className="footer-text-list"
        >
          {t("footer.more_product")}
        </p>
        <p
          onClick={() => openUrl("https://right-com.com/platform/")}
          className="footer-text-list"
        >
          {t("footer.go_to_xp")}
        </p>
      </div>

      <div className="footer-logo-container">
        <p className="footer-text-list" style={{ cursor: "default" }}>
          {t("footer.powred_by")}
        </p>
        <img src={logoRC} alt="logo-right-com" style={{ height: "30%" }} />
      </div>
    </div>
  );
};

export default MainFooter;
