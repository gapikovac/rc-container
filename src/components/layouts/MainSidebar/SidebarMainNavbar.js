import React from "react";
import PropTypes from "prop-types";
import { Navbar, NavbarBrand } from "shards-react";

import { Dispatcher, Constants } from "../../../flux";

const leftMenuLogoContainer = {
  padding: "1.45rem 0 1.45rem 1.5rem",
  borderBottom: "1px solid $secondary-nav-border",
  display: "flex",
  alignItems: "center"
};

const leftMenuLogoText = {
  color: "#212121",
  fontSize: "1.5rem",
  fontWeight: "bold"
};

const navbarItem = {
  display: "flex",
  alignItems: "center"
};

class SidebarMainNavbar extends React.Component {
  constructor(props) {
    super(props);

    this.handleToggleSidebar = this.handleToggleSidebar.bind(this);
  }

  handleToggleSidebar() {
    Dispatcher.dispatch({
      actionType: Constants.TOGGLE_SIDEBAR
    });
    //src={require("../../../assets/images/logo/Rightcare_logo.svg")}
  }

  render() {
    const { hideLogoText } = this.props;
    return (
      <div className="left-menu-logo-container">
        <a className="navbar-item" href="/">
          <img
            alt="logo icon"
            src={require("../../../assets/images/logo/Rightcare_logo.svg")}
          />
        </a>
        <p className="left-menu-logo-text">RightCare</p>
      </div>
    );
  }
}

SidebarMainNavbar.propTypes = {
  /**
   * Whether to hide the logo text, or not.
   */
  hideLogoText: PropTypes.bool
};

SidebarMainNavbar.defaultProps = {
  hideLogoText: false
};

export default SidebarMainNavbar;
