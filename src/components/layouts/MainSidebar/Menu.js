import React, { useRef, useState, useEffect, useContext } from "react";
import { NavLink, useRouteMatch } from "react-router-dom";
import { Nav } from "shards-react";

import PropTypes from "prop-types";
import DashboardIcon from "../../../assets/images/dashboard/menu/dashboard.svg";
import AgentIcon from "../../../assets/images/dashboard/menu/agent.svg";
import TicketIcon from "../../../assets/images/dashboard/menu/ticket.svg";
import ChannelIcon from "../../../assets/images/dashboard/menu/channel.svg";
import ProfileIcon from "../../../assets/images/dashboard/menu/profile.svg";
import SettingIcon from "../../../assets/images/dashboard/menu/setting.svg";
import LogoutIcon from "../../../assets/images/dashboard/menu/logout.svg";
import ActiveDashboardIcon from "../../../assets/images/dashboard/menu/active/dashboard-active.svg";
import ActiveProfileIcon from "../../../assets/images/dashboard/menu/active/profile-active.svg";
import ActiveAgentIcon from "../../../assets/images/dashboard/menu/active/agent-active.svg";
import ActiveSettingIcon from "../../../assets/images/dashboard/menu/active/setting-active.svg";
import ActiveLogoutIcon from "../../../assets/images/dashboard/menu/active/logout-active.svg";
import ActiveTicketIcon from "../../../assets/images/dashboard/menu/active/ticket-active.svg";
import ActiveChannelIcon from "../../../assets/images/dashboard/menu/active/channel-active.svg";
import useHttpService from "../../../services/HttpService";
import useRouter from "../../app/useRouter";
import { SharedDataContext } from "../../app/UseContext";

import SidebarNavItem from "./SidebarNavItem";
import { Store } from "../../../flux";

const Menu = props => {
  const { path } = useRouteMatch();
  const dashboardLink = React.createRef();
  const profileLink = React.createRef();
  const settingLink = React.createRef();
  const agentLink = React.createRef();
  const ticketLink = React.createRef();
  const channelLink = React.createRef();
  const logoutLink = React.createRef();

  const router = useRouter();
  const { sharedDataContext, setSharedDataContext } = useContext(
    SharedDataContext
  );
  const t = sharedDataContext.t;
  const {
    send: sendRequestCheckSession,
    data: sessionResponse,
    //loading: onLoadingSession,
    error: onErrorSession
  } = useHttpService("LOGOUT");

  const currentDashboardIcon =
    path === "/dashboard" ? ActiveDashboardIcon : DashboardIcon;
  const currentTicketIcon = path === "/tickets" ? ActiveTicketIcon : TicketIcon;
  const currentSettingIcon =
    path === "/settings" ? ActiveSettingIcon : SettingIcon;
  const currentLogoutIcon = path === "/logout" ? ActiveLogoutIcon : LogoutIcon;

  const hasMount = useRef(false);
  const unmounted = useRef(false);
  const [stateCheckSession, setStateCheckSession] = useState("loading");
  const [ticketRoute, setTicketRoute] = useState(false);
  useEffect(() => {
    if (!unmounted.current) {
      if (onErrorSession) {
        setStateCheckSession("error");
      }
      if (sessionResponse) {
        if (sessionResponse.status === 200) {
          //   console.log("sessionResponse", sessionResponse);
          // set user session
          // updateUserInfo(sessionResponse.user);
          //updatecontext(sessionResponse.user);
        }
      }
    }
  }, [sessionResponse]);

  const handleMouseOut = kind => {
    switch (kind) {
      case "dashboard":
        dashboardLink.current.src =
          path === "/dashboard" ? ActiveDashboardIcon : DashboardIcon;
        break;
      case "profile":
        profileLink.current.src =
          path === "/profile" ? ActiveProfileIcon : ProfileIcon;
        break;
      case "setting":
        settingLink.current.src =
          path === "/settings" ? ActiveSettingIcon : SettingIcon;
        break;
      case "agent":
        agentLink.current.src =
          path === "/agents" ? ActiveAgentIcon : AgentIcon;
        break;
      case "ticket":
        ticketLink.current.src =
          path === "/tickets" ? ActiveTicketIcon : TicketIcon;
        break;
      case "channel":
        channelLink.current.src =
          path === "/channels" ? ActiveChannelIcon : ChannelIcon;
        break;
      case "logout":
        logoutLink.current.src =
          path === "/logout" ? ActiveLogoutIcon : LogoutIcon;
        break;
      default:
        break;
    }
  };

  function logout() {
    let url = window.location.href;
    let tab = url.split(".");
    let alias = tab[0];
    return window.open(`${alias}.account.${tab[2]}.com/logout`, "_self");
  }

  function reloaded() {
    // setSharedDataContext({
    //   ...sharedDataContext,
    //   status: 0
    // })
    setTimeout(() => {
      setSharedDataContext({
        ...sharedDataContext,
        status: 0,
        notification: {
          active: false
        }
      });
    }, 2500);
  }

  function ticketRedirect() {
    let url = window.location.href;
    let tab = url.split("/");
    let alias = tab[tab.length - 1];
    // console.log(alias)
    return alias !== "tickets" || sharedDataContext.status === 1
      ? setSharedDataContext({
          ...sharedDataContext,
          status: 0
        })
      : reloaded();
  }

  const handleMouseOver = kind => {
    switch (kind) {
      case "dashboard":
        dashboardLink.current.src = ActiveDashboardIcon;
        break;
      case "profile":
        profileLink.current.src = ActiveProfileIcon;
        break;
      case "setting":
        settingLink.current.src = ActiveSettingIcon;
        break;
      case "agent":
        agentLink.current.src = ActiveAgentIcon;
        break;
      case "ticket":
        ticketLink.current.src = ActiveTicketIcon;
        break;
      case "channel":
        channelLink.current.src = ActiveChannelIcon;
        break;
      case "logout":
        logoutLink.current.src = ActiveLogoutIcon;
        break;
      default:
        break;
    }
  };

  return (
    <div className="menu-container">
      <p className="menu-label">{t("side_menu.menu")}</p>
      <ul className="menu-list">
        <li>
          <NavLink
            activeClassName="selected"
            className="sidelink"
            to="dashboard"
            onFocus={e => e}
            onBlur={e => e}
          >
            <img
              alt="dash icon"
              src={currentDashboardIcon}
              ref={dashboardLink}
            />
            {t("side_menu.dashboard")}
          </NavLink>
        </li>
        <li>
          <NavLink
            activeClassName="selected"
            className="sidelink"
            to="tickets"
            onClick={() => ticketRedirect()}
          >
            <img alt="ticket icon" src={currentTicketIcon} ref={ticketLink} />
            {t("side_menu.tickets")}
          </NavLink>
        </li>
        <li>
          <NavLink
            activeClassName="selected"
            className="sidelink"
            to="settings"
            onFocus={e => e}
            onBlur={e => e}
          >
            <img alt="set icon" src={currentSettingIcon} ref={settingLink} />
            {t("side_menu.settings")}
          </NavLink>
        </li>
        <li>
          <NavLink
            activeClassName="selected"
            className="sidelink"
            to="logout"
            onFocus={e => e}
            onBlur={e => e}
            onClick={() => logout()}
          >
            <img alt="log icon" src={currentLogoutIcon} ref={logoutLink} />
            {t("side_menu.logout")}
          </NavLink>
        </li>
      </ul>
    </div>
  );
};

export default Menu;
