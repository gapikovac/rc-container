// Define and handle all routes related to api
const express = require('express');

const router = express.Router();
const CONSTANTS = require('../constants');
const fakeService = require('./fakeService');
const helpers = require('./helpers');
const CheckSession = require('./checkSession');

const redirectToLogout= function (req, res, alias) {
  // console.log('currentUrl : ', currentUrl);
  
  const redirectUrlLogout = `${CONSTANTS.ACCOUNT_SERVICE.PROTOCOL}${alias}.${CONSTANTS.ACCOUNT_SERVICE.BASE_URL}${CONSTANTS.ACCOUNT_SERVICE.LOGOUT}`;

  console.log('Redirecting to : ', redirectUrlLogout);

  res.redirect(redirectUrlLogout);
};


// route to check current session
router.get('/api/decoder', (req, res, next) => {
  console.log('run ==> decoder');
  if (!CONSTANTS.FAKE_CHECK_SESSION) { /** refact */
    const { hostname } = req;
    const hostname_part = hostname.split('.');
    const accountAlias = CONSTANTS.USE_CLIENT_ALIAS === true
      ? hostname_part[0]
      : CONSTANTS.DEV_ALIAS;
    if (req.cookies && req.cookies[CONSTANTS.SESSION_SERVICE.KEY]) {
      console.log(`decode ${req.cookies[CONSTANTS.SESSION_SERVICE.KEY]}`);
     let apisid = req.cookies[CONSTANTS.SESSION_SERVICE.KEY]
      new CheckSession(
        CONSTANTS.SESSION_SERVICE.KEY,
        apisid,
        accountAlias,
        
      )
        .execute()
        .then((response_check_apisid) => {
          if (
            response_check_apisid
            && response_check_apisid.status === 200
            && response_check_apisid.data
          ) {
            // session is ok
            console.log('response_check_apisid = ', response_check_apisid);
           let userData = helpers.buildUserSession(response_check_apisid)
           userData.user.apisid = apisid
           res
              .status(200)
              .send(userData);
          } else if (
            response_check_apisid
            && (response_check_apisid.status === 403
              || response_check_apisid.status === 401)
          ) {
            res.status(200).send({
              status: response_check_apisid.status,
              data: null,
              message: 'Session not found',
            });
          } else {
            res.status(200).send({
              status: 500,
              data: null,
              message: 'Internal server error',
            });
          }
        })
        .catch((errc) => {
          res.status(200).send({
            status: 500,
            data: null,
            message: 'Internal server error',
          });
        });
    } else {
      console.log('* Decoder : No cookie found');
      res.status(200).send({
        status: 404,
        data: null,
        message: 'No session found',
      });
    }
  } else {
    console.log('* Decoder FAKE_CHECK_SESSION');

    const data = fakeService.fakeCheckSession();
    // console.log('* data User : ', data);

    res.status(200).send(helpers.buildUserSession(data));
  }
});

// route to LOGOUT
router.get('/api/logout', (req, res, next) => {
  console.log('run ==> decoder logout');
  console.log('run ==> decoder logout',req);
  if (!CONSTANTS.FAKE_CHECK_SESSION) { /** refact */
    const { hostname } = req;
    const hostname_part = hostname.split('.');
    const accountAlias = CONSTANTS.USE_CLIENT_ALIAS === true
      ? hostname_part[0]
      : CONSTANTS.DEV_ALIAS;

      // const redirectUrlLogout = `${CONSTANTS.ACCOUNT_SERVICE.PROTOCOL}${accountAlias}.${CONSTANTS.ACCOUNT_SERVICE.BASE_URL}${CONSTANTS.ACCOUNT_SERVICE.LOGOUT}`;
      //
      // axios
      // .get(redirectUrlLogout)
      // .then((response) => {
      //   console.log('request check session response.data = ', response.data);
       
      //   resolve(response.data);
        
      // })
      // .catch((error) => {
      //   console.log('error request check session ', error);

      //   reject(error);
      // });
      redirectToLogout(req, res, accountAlias);
  } else {
    console.log('* Decoder FAKE_CHECK_SESSION LOGOUT');

   // const data = fakeService.fakeCheckSession();
    // console.log('* data User : ', data);
  //  redirectToLogout(req, res, 'rightcom');
  //  res.status(200).send(helpers.buildUserSession(data));
  }
});
module.exports = router;
