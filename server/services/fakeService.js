// all fake service will be defined here

module.exports.fakeCheckSession = function () {
    const userData = {
        status: 200,
        /*
        data: {
          address: "",
          alias: "MB1O5HSOK97W",
          city: "",
          company: "RightCom Technologies",
          country: "BJ",
          email: "demo@right-com.com",
          firstlogin: 1,
          firstname: "RightCom",
          full_rights: [],
          image: null,
          is_admin: true,
          is_owner: false,
          language: "fr",
          lastname: "Technologies",
          phone: "(+229) 66 28 68 95",
          publickey: "POKB19302SOK97W",
          rights_codes: [],
          session_id: "SdYRKYXaOR2P1oJUz2bTRIp8KEyzvJEJtUZJSoPx5SOCb",
          sexe: "M",
          status: 1,
          subdomain: "rightcom",
          timezone: "GMT + 1",
          user_apps: ["admin", "rightplayer", "rightq", "capture", "rightflow"],
          user_id: "504737b0-ff19-11e8-9f81-a3a55915c277"
        }
        */

        data: {
            address: "",
            alias: "MB1O5HSOK97W",
            city: "",
            company: "RightCom Technologies",
            country: "CI",
            email: "ange@right-com.com",
            firstlogin: 1,
            firstname: "Gnaba",
            full_rights: [],
            image: "",
            is_admin: true,
            is_owner: false,
            language: "fr",
            lastname: "Ange Patrick",
            phone: "",
            publickey: "POKB19302SOK97W",
            rights_codes: [],
            session_id: "Ez9alWojt4QNs4uRDWhKbIQ5BOGmsgCv0YN7PgjsnWujq",//"fIrgwKdrCj4QHMJhXuDpFwd5f5sloyLV7oncJVgLAxlnQ",
            apisid: "mvGMiWVsirOd8n3VgOCtH9v97rKD4tti1CT1yoGK",//"lP6Pdh4FjGY2I6zHG10s4kWsRQIfP8tFxOKsJlci",
            sexe: "M",
            status: 1,
            subdomain: "rightcom",
            timezone: "GMT+00:00",
            user_apps: [
                "admin",
                "rightplayer",
                "rightq",
                "capture",
                "rightflow",
                "rightcare"
            ],
            rc_microfrontend_version: {
                onboard: "v1",
                dashboard: "v1",
                tickets: "v1",
                settings: "v1"
            },
            user_id: "a9c6b8c0-36a8-11ea-8485-fd57cda550da"//"df61ddc0-36a8-11ea-8485-fd57cda550da",
        }
    };
    return userData;
};
