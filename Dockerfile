
# node:12-alpine ; node:carbon
FROM node:carbon

# Create app directory
WORKDIR /usr/src/rightcare-web-xp

# Install app dependencies

# Copy dependencies files
COPY package.json yarn.lock ./
RUN yarn install

# Bundle app source
COPY . .

# Build app
## RUN yarn build && yarn global add serve
RUN yarn build

# Remove unusual files for running
RUN rm -rf ./src ./public

# Your app binds to port 305 so you�ll use the EXPOSE instruction to have it mapped by the docker daemon:
EXPOSE 9000

## CMD ["serve", "-s", "build", "-l", "9000"]
CMD ["node", "index.js"]
